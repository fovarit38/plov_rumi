angular.module('rumiApp')
    .controller('MainController', MainController)
    .directive('setClassWhenAtTop', function ($window) {
    var $win = angular.element($window);

    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            var topClass = attrs.setClassWhenAtTop,
                offsetTop = 300;

            $win.on('scroll', function (e) {
                if ($win.scrollTop() >= offsetTop) {
                    element.addClass(topClass);
                } else {
                    element.removeClass(topClass);
                }
            });
        }
    };
}).

controller('ctrl', function ($scope) {
    $scope.scrollTo = function (target){
    };
});



function MainController($http, $scope, $document) {

    mn = this;
    window.mn = mn;

    mn.closeMenu = function () {
        mn.mainmenu = false;
    };

    mn.closeAuthForm = function () {
        mn.authform = false;
    };

    mn.closeCartForm = function () {
        
        mn.cartform = false;
    };
    
    mn.closeLoggedForm = function () {
        mn.logged = false;
    };

    mn.closeFixedCart = function () {
        mn.cartformFix = false;
    };
}
