angular.module('rumiApp').controller('CartController', CartController).controller('HomeController', HomeController);

function HomeController($http) {
    hm = this;
    window.hm = hm;
    (function getPopularProducts() {
        $http.get('/popular/products/get').then(function (response) {
            hm.popularProducts = response.data;
        });
    }())
}

function CartController($http, $scope, $timeout, localStorageService, $location) {
    ct = this;
    window.ct = ct;

    ct.code = null;
    ct.promocode = null;
    ct.cartsumm = null;
    ct.deliverysumm = 0;
    ct.totalSumm = null;
    ct.square = null;
    ct.delivery = 1;
    ct.priceis = 0;
    ct.pickup = 0;

    $timeout(function () {

        orderSumm();
        productCount();
        getTotalSumm();
    });

    ct.applyPromocode = function () {
        $http.get('/apply-promocode', {params: {code: ct.code}}).then(
            function (response) {
                ct.errorData = null;
                ct.promocode = response.data.promocode;
                getTotalSumm();
            },
            function (error) {

                ct.promocode = null;
                if (error.status == 422) {
                    ct.errorData = 'Введите пожалуйста промокод';
                }
                if (error.status == 400){
                    ct.errorData = error.data;
                }

                getTotalSumm();
            }
        );
    };


    function delsumm() {
        if (ct.cartsumm >= 7000) {
            ct.deliverysumm = 0;

            switch (ct.square) {
                case 2:
                    ct.deliverysumm = 1000;
                    break;
                case 3:
                    ct.deliverysumm = 1500;
                    break;
                case 4:
                    ct.deliverysumm = 2000;
                    break;
            }
        }
        if (ct.cartsumm < 7000) {
            switch ($scope.square) {
                case 1:
                    ct.deliverysumm = 500;
                    break;
                case 2:
                    ct.deliverysumm = 1000;
                    break;
                case 3:
                    ct.deliverysumm = 1500;
                    break;
                case 4:
                    ct.deliverysumm = 2000;
                    break;
            }
        }

        getTotalSumm();
    }

    ct.isEmpty = function (obj) {
        for (var prop in obj) {
            if (obj.hasOwnProperty(prop)) return false;
        }
        return true;
    };
    if (!localStorageService.get('cart')) {
        localStorageService.set('cart', JSON.stringify({}));
    }

    ct.cart = JSON.parse(localStorageService.get('cart'));

    ct.repeatOrder = function (id) {
        ct.clearSession();
        $http.post('/repeat/order/' + id + '').then(function (response) {
            localStorageService.set('cart', JSON.stringify(response.data));
            window.location = "/cart";
        });
    };

    ct.addSingleProduct = function (id) {
        $http.post('/product/' + id + '/add').then(function (response) {
            ct.singleProduct = response.data;
        });
    };
    ct.addToCart = function (product) {
        if (ct.cart[product.id]) {
            ct.cart[product.id].quantity++;
        } else {
            ct.cart[product.id] = {
                quantity: 1,
                id: product.id,
                iiko_id: product.iiko_id,
                title: product.title,
                price: product.price,
                image: product.main_image,
                weight: product.weight
            };
        }
        localStorageService.set('cart', JSON.stringify(ct.cart));

        orderSumm();
        productCount();
        getTotalSumm();
    };

    ct.delete = function (id) {
        delete ct.cart[id];
        localStorageService.set('cart', JSON.stringify(ct.cart));

        orderSumm();
        productCount();
        getTotalSumm();
    };

    ct.minus = function (product) {
        if (ct.cart[product.id]) {
            ct.cart[product.id].quantity--;
            if (ct.cart[product.id].quantity == 0) {
                ct.delete(ct.cart[product.id].id);
            }
        }
        localStorageService.set('cart', JSON.stringify(ct.cart));
        orderSumm();
        productCount();
        getTotalSumm();

    };

    function getTotalSumm() {
        ct.totalSumm = ct.cartsumm + ct.deliverysumm;

        if (ct.promocode) {

            if (ct.promocode.type == 'discount') {
                ct.totalSumm = ct.totalSumm - (ct.totalSumm * (ct.promocode.discount / 100))
            }
            if (ct.promocode.type == 'sum') {
                ct.totalSumm = ct.totalSumm - ct.promocode.sum;
            }
            if (ct.promocode.type == 'delivery') {
                ct.deliverysumm = 0;
                ct.totalSumm = ct.cartsumm + ct.deliverysumm;
            }
        }
        ct.totalSumm  = Math.floor(ct.totalSumm);
    }

    function orderSumm() {
        var result = 0;
        for (var key in ct.cart) {
            var summa = ct.cart[key].price * ct.cart[key].quantity;
            result += summa;
        }
        ct.cartsumm = result;
    }

    function productCount() {
        ct.cartProductCount = Object.keys(ct.cart).length;
    }

    ct.setSquare = function (summa) {
        ct.deliverysumm = summa;
        getTotalSumm();
    };

    window.setsumm=function (summa){
        ct.deliverysumm = summa;
        getTotalSumm();
    }

    ct.clearSession = function () {
        localStorageService.set('cart', undefined);
    };


}