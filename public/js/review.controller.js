angular.module('rumiApp')
    .controller('ReviewsController', ReviewsController);


function ReviewsController($http, notify) {

    getErrorMessage = function () {
        return 'Извините произошла ошибка'
    };

    rv = this;
    window.rv = rv;
    rv.rating = null;

    rv.save = function (id) {

        if (!rv.rating)
        {
            notify({message: 'Поставьте пожалуйста оценку', position: 'center', classes: 'alert-danger', duration: 22000});
        }

        $http.post('/product/' + id + '/review/save', {

            text: rv.textReview,
            rating: rv.rating

        }).then(function (response) {
            notify({message: response.data, position: 'center', classes: 'alert-success', duration: 3000});
            rv.textReview = '';
            rv.rating = null;
        });
    };


    rv.reviews = [];

    rv.getReviews = function (id) {
        $http.get('/product/' + id + '/reviews/get').then(function (response) {
            rv.reviews = response.data;
        }, function () {
            notify({message: getErrorMessage, position: 'right', classes: 'alert-danger', duration: 3000});
        });

    };

    function lastReviews() {
        $http.get('/recent/reviews/get').then(function (response) {
            rv.recentReviews = response.data;
        })
    }
    
    lastReviews();
}