angular.module('adminRumi').controller('IntegrationController', IntegrationController);

function IntegrationController($http, notify) {

    ig = this;
    window.ig = ig;

    ig.result = '';

    ig.integration = function (city) {
        $http.get('/get/nomenclature/' + city +'').then(function (response) {
            switch (response.status) {
                case (200):
                    notify('Интеграция прошла успешна');
                    break;
                default:
                    ig.result = '';
            }
        }, function (response) {
            switch (response.status) {
                case (500):
                    notify({
                        message: 'Произошла ошибка повторите попытку позже',
                        position: 'center',
                        classes: 'alert-danger',
                        duration: 3000
                    });
                    break;
                default:
                    ig.result = '';
            }
        });
    }

}