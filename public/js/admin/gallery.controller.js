angular.module('adminRumi')
    .controller('GalleryController', GalleryController);

function GalleryController($http, Upload) {

    gl = this;
    window.gl = gl;

    gl.singleImage = null;

    gl.getImage = function (type, id) {
        $http.get('/' + type + '/' + id + '/image/get').then(
            function (response) {
                gl.singleImage = response.data;
            },
            function (error) {
            });
    };


    gl.uploadBanner = function (files, $object_id, $object_type) {

        angular.forEach(files, function (file) {

            file.upload = Upload.upload({
                url: '/' + $object_type + '/' + $object_id + '/image/upload',
                data: {
                    file: file,
                    object_id: $object_id,
                    object_type: $object_type
                }
            });

            file.upload.then(function (response) {
                if (response.data.error) {
                    alert(response.data.error);
                    return;
                }
                gl.singleImage = response.data;
            });
        });
    };

    gl.deleteBanner = function (image, $object_id, $object_type) {
        $http.post('/' + $object_type + '/' + $object_id + '/image/' + image.id + '/delete').then(
            function (response) {
                gl.singleImage = null;
            })
    };


    gl.getImages = function (type, id) {
        $http.get('/' + type + '/' + id + '/images/get').then(
            function (response) {
                gl.images = response.data;
            },
            function (error) {
            });
    };

    gl.uploadImages = function (files, $object_id, $object_type) {

        angular.forEach(files, function (file) {

            file.upload = Upload.upload({
                url: '/' + $object_type + '/' + $object_id + '/image/upload',
                data: {
                    file: file,
                    object_id: $object_id,
                    object_type: $object_type
                }
            });

            file.upload.then(function (response) {
                if (response.data.error) {
                    alert(response.data.error);
                    return;
                }
                gl.images.push(response.data);
            });
        });
    };


    gl.deleteImage = function (image, $object_id, $object_type) {
        $http.post('/' + $object_type + '/' + $object_id + '/image/' + image.id + '/delete').then(function (response) {
            for (i = 0; i < gl.images.length; i++) {
                if (gl.images[i].id == image.id)
                    gl.images.splice(i, 1);
            }
        })
    }


}

