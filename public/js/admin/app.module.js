angular.module('adminRumi',
    [
        'cgNotify',
        'ngFileUpload',
        'angular-loading-bar'
    ]).config(['cfpLoadingBarProvider', function (cfpLoadingBarProvider) {
        cfpLoadingBarProvider.includeSpinner = true;
    }]);

