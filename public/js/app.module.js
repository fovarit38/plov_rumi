angular.module('rumiApp',
    [
        'LocalStorageModule',
        'cgNotify',
        'ngFileUpload',
        'ngImgCrop',
        'angular-click-outside',
        'ui.mask'
    ])
    .config(function (localStorageServiceProvider) {
        localStorageServiceProvider.setPrefix('rumi');
    })
;

