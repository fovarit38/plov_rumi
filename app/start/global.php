App::error(function(Exception $exception, $errorCode)
{
    $requestUrl = Request::fullUrl(); 
    $userAgent = Request::header('user-agent');

    if($errorCode != 404){
        Log::error('Exception', array(
            'errorCode' => $errorCode,
            'requestUrl' => $requestUrl,
            'userAgent' => $userAgent,
            'context' => $exception,
        ));     
    }

    return Response::view('error-page-path.error-404', array(), 404);
    // Here "error-404" is a blade view file in "error-page-path" directory
});