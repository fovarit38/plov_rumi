MERCHANT_CERTIFICATE_ID = "c183eeba"				; Серийный номер сертификата Cert Serial Number
MERCHANT_NAME = "plov.kz"				; Название магазина (продавца) Shop/merchant Name
PRIVATE_KEY_FN = "../app/kkb/cert.prv"				; Путь к закрытому ключу Private cert path
PRIVATE_KEY_PASS = "WDfUveEf9i3"					; Пароль к закрытому ключу Private cert password
XML_TEMPLATE_FN = "../app/kkb/template.xml"			; Путь к XML шаблону XML template path
XML_COMMAND_TEMPLATE_FN = "../app/kkb/command_template.xml"	; Путь к XML шаблону для команд (возврат/подтверждение) 
PUBLIC_KEY_FN = "../app/kkb/kkbca.pem"				; Путь к открытому ключу Public cert path
MERCHANT_ID="93162311"						; Терминал ИД в банковской Системе