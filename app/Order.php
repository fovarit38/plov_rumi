<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    //

    protected $casts = [
        'delivery' => 'boolean',
        'status' => 'boolean'
    ];

    public function products()
    {
        return $this->belongsToMany(Product::class)->withPivot('qty');
    }

    public function getDateAttribute()
    {
        $date = new Carbon($this->created_at);

        return $date->format('j') . ' ' . trans('months.' . $date->format('F') . '') . ', ' . $date->format('Y') . ' г.' . ' ' . $date->format('H:i');
    }


    public function promocode()
    {
        return $this->belongsTo(Promocode::class);
    }

}
