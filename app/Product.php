<?php

namespace App;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Product extends Model
{
    //
    public $appends = ['image_url' , 'main_image'];
    protected $fillable = ['iiko_id'];

    public function category()
    {
        return $this->belongsTo(Category::class ,'category_id' , 'iiko_id');
    }

    public function recommend()
    {
        return $this->belongsToMany(Product::class, 'product_recommend', 'product_id', 'recommend_id');
    }

    public function getMainImageAttribute()
    {
        $image = Image::where('object_id' , $this->id)->where('object_type', 'product')->first();

        return $image ? $image->url : '/app/voyager/placeholder.png';
    }

    public function images()
    {
        return $this->hasMany(Image::class, 'object_id');
    }

    public function getImageUrlAttribute()
    {
        return $this->image ? $this->image : 'placeholder.png';
    }

    public function reviews()
    {
        return $this->hasMany(Review::class);
    }

    public function getRatingAttribute()
    {
        return round($this->reviews->avg('rating'), 0);
    }

    public function getRatingStarsAttribute()
    {
        for ($x = 1; $x <= $this->rating; $x++) {
            echo '<i class="rating_star "></i>';
        }
        if (strpos($this->rating, '.')) {
            echo '<i class="rating_star_empty"></i>';
            $x++;
        }
        while ($x <= 5) {
            echo '<i class="rating_star_empty"></i>';
            $x++;
        }
    }

}
