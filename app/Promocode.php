<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Promocode extends Model
{
    //
    protected $dates = [
        'end_date'
    ];

    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }

    public function productId()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }

}
