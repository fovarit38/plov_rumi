<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    //

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function getRatingStarsAttribute()
    {
        for ($x = 1; $x <= $this->rating; $x++) {
            echo '<i class="rating_star "></i>';
        }
        if (strpos($this->rating, '.')) {
            echo '<i class="rating_star_empty"></i>';
            $x++;
        }
        while ($x <= 5) {
            echo '<i class="rating_star_empty"></i>';
            $x++;
        }
    }
}
