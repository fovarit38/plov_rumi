<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function reviews()
    {
        return $this->hasMany(Review::class);
    }

    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    function socialProviders()
    {
        return $this->hasMany(SocialProvider::class);
    }

    function getDateRegisterAttribute()
    {
        $date = new Carbon($this->created_at);
        return $date->format('j') .' '. trans('months.'.$date->format('F').''). ', ' . $date->format('Y').' г.';
    }
}
