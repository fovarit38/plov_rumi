<?php

namespace App\Http\Controllers;

use App\Category;
use App\City;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;

class CatalogController extends Controller
{

    public function index($slug)
    {
        $category = Category::where('slug', $slug)->where('city', City::getCookieCity())->where('active', 1)->first();

        

        return view('catalog.index', compact('category', 'products'));
    }

    public function single($category_slug, $slug)
    {
        $product = Product::where('slug' , $slug)->where('city' , City::getCookieCity())->first();
        
        if (!$product)
        {
            return redirect('/');
        }
        return view('catalog.single', compact('product'));
    }
    
}
