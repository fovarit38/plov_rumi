<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserController extends Controller
{
    //

    public function registration()
    {
        return view('auth.register');
    }

    public function resetPass()
    {
        return view('auth.passwords.email');
    }
    
}
