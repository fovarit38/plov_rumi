<?php

namespace App\Http\Controllers;

use App\Category;
use App\City;
use App\Helpers;
use App\Order;
use App\Product;
use App\Review;
use App\Slider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cookie;
use TCG\Voyager\Models\Page;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        foreach (Order::all() as $item){
            if ($item->delivery){
                $item->delivery = 1;
                $item->save();
            }
            else{
                $item->delivery = 0;
                $item->save();
            }
        }


        $categories = Category::where('active', 1)->get();
        $reviews = Review::where('published', 1)->with('user')->limit(9)->orderBy('created_at', 'desc')->get();
        $products = Product::where('in_slider', 1)->where('active', 1)->where('city', City::getCookieCity())->get();
        $sliderItems = Slider::where('active', 1)->get();

        return view('page.home', compact('categories', 'reviews', 'products', 'sliderItems'));
    }


    public function popularProducts()
    {
        $popularProducts = Product::where('in_popular', 1)->where('city', City::getCookieCity())->where('active', 1)->with('category')->get();

        return response()->json($popularProducts);
    }

}
