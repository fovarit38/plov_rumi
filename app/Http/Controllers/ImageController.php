<?php

namespace App\Http\Controllers;

use App\Image;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class ImageController extends Controller
{


    private $newImageFileName;


    public function getImages(Request $request, $type,$id)
    {
        $images = Image::where('object_id', $id)->where('object_type', $type)->get();
        return response()->json($images);
    }

    public function getImage(Request $request, $type, $id)
    {
        $image = Image::where('object_id', $id)->where('object_type', $type)->first();
        return $image;
    }

    public function upload(Request $request)
    {

        $file = Input::file('file');
        if ($error = $this->validateImageUpload($file))
            return response()->json(['error' => $error]);

        $imagePath = $this->doUploadGalleryImg($request->object_id, $file, $request->object_type);
        $newImage = $this->createNewImageEntry($request->object_id, $imagePath, $request->object_type);

        return response()->json(['id' => $newImage->id, 'url' => $newImage->url, 'message' => 'Вы успешно загрузили ']);
    }

    protected function validateImageUpload($file)
    {
        $validator = \Validator::make(['image' => $file], ['image' => 'mimes:jpeg,jpg,png|required']);

        if ($validator->fails()) {
            return 'Not an image.';
        }

        return false;
    }

    protected function doUploadGalleryImg($objectId, $file, $object_type )
    {
        $imagePath = '/app/voyager/products/' . $objectId . '/';
        $destinationPath = public_path() . $imagePath;

        
        $file->move($destinationPath, $this->getNewImageFileName());

        return $imagePath;
    }

    protected function createNewImageEntry($objectId, $imagePath, $objectType)
    {
        $image = new Image();
        $image->object_id = $objectId;
        $image->object_type = $objectType;
        $image->url = $imagePath . $this->getNewImageFileName();
        $image->save();

        return $image;
    }


    public function deleteGalleryImg($objectType, $objectId, $imgId)
    {
        $image = Image::where('id', $imgId)->where('object_id', $objectId)->where('object_type', $objectType);

        $this->deleteGalleryImgFile($image);
        $this->deleteGalleryImgEntry($image);

    }

    protected function deleteGalleryImgEntry(Builder $image)
    {
        $image->delete();
    }

    protected function deleteGalleryImgFile(Builder $gallery_img)
    {
        if ($image = $gallery_img->first())
            \File::delete(trim($image->url, '/'));
    }

    protected function getNewImageFileName()
    {
        if (!$this->newImageFileName)
            $this->newImageFileName = round(microtime(true) * 1000) . '.jpg';
        return $this->newImageFileName;

    }
}
