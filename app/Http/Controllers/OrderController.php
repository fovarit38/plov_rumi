<?php

namespace App\Http\Controllers;

use App\City;
use App\Helpers;
use App\Order;
use App\Product;
use App\Promocode;
use App\Transaction;
use Carbon\Carbon;
use Dosarkz\EPayKazCom\Facades\Epay;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class OrderController extends Controller
{
  
    public function newOrder(Request $request)
    {

        $user = \Auth::user();
        $products = json_decode($request->products, true);

        $promocode = Promocode::where('code', $request->promocode)->where('city', City::getCookieCity())->where('end_date', '>=', Carbon::now())->first();

        $order = new Order();

        if (\Auth::guest()) {
            $order->user_id = null;
        } else {
            $order->user_id = $user->id;
        }

        $order->name = $request->name;
        $order->phone = $request->phone;
        $order->email = $request->email;
        $order->amount = $request->amount;
//        $order->delivery_amount = (($request->square == 1 || $request->square == 6 || $request->square == 5) && $request->amount >= 10000) || !$request->delivery ? 0 : Helpers::$squarePrice[$request->square];
        $order->delivery_amount = $request->delivery_amount;
        $order->delivery = $request->delivery;
        $order->total_amount = $order->amount + $order->delivery_amount;
        $order->street = $request->street;
        $order->home = $request->home;
        $order->apartment = $request->apartment;
        $order->entrance = $request->entrance;
        $order->floor = $request->floor;
        $order->doorphone = $request->doorphone;
        $order->payment = $request->payment;
        $order->zdacha = $request->zdacha;
        $order->square = $request->square;
        $order->time = $request->time;
        $order->status = false;
        $order->city = City::getCookieCity();

        $order->save();

        if ($promocode) {

            $summ = 0;
            foreach ($products as $product) {
                $summ = $summ + ($product['quantity'] * $product['price']);
            }
            $order->amount = $summ;

            if ($promocode->type == 'discount') {
                $order->total_amount = ($order->amount + $order->delivery_amount) - intval(($order->amount + $order->delivery_amount) * ($promocode->discount / 100));
            }

            if ($promocode->type == 'sum') {
                $order->total_amount = ($order->amount + $order->delivery_amount) - $promocode->sum;
            }

            if ($promocode->type == 'delivery') {
                $order->delivery_amount = 0;
                $order->total_amount = $order->amount + $order->delivery_amount;
            }

            $order->promocode_id = $promocode->id;
            $order->save();
        }

        foreach ($products as $product) {
            $order->products()->attach($product['id'], ['qty' => $product['quantity']]);
        }

        if ($request->payment == 'online') {

            $transaction = new Transaction();
            $transaction->order_id = $order->id;
            $transaction->amount = $order->total_amount;
            $transaction->is_confirmed = false;
            $transaction->save();

            $pay = Epay::basicAuth([
                'order_id' => $order->id,
                'currency' => '398',
                'amount' => $order->total_amount,
                'email' => $order->email,
                'hashed' => true,
            ]);

            if ($pay->generateUrl()) {
                return Redirect::to($pay->generateUrl());
            }
            return false;
        }

        $this->orderForEmail($order);


        return Redirect::route('orderaccepted', ['id' => $order->id]);
    }

    public function postlink(Request $request)
    {
        \Log::alert($request->all());
        $response = $request->input('response');
        $path1 = '../app/epay/config.txt';
        $result = process_response(stripslashes($response), $path1);

        if (is_array($result)) {
            if (in_array("ERROR", $result)) {
                if ($result["ERROR_TYPE"] == "ERROR") {
                    Log::info("System error:" . $result["ERROR"]);
                } elseif ($result["ERROR_TYPE"] == "system") {
                    Log::info("Bank system error > Code: '" . $result["ERROR_CODE"] . "' Text: '" . $result["ERROR_CHARDATA"] . "' Time: '" . $result["ERROR_TIME"] . "' Order_ID: '" . $result["RESPONSE_ORDER_ID"] . "'");
                } elseif ($result["ERROR_TYPE"] == "auth") {
                    Log::info("Bank system user autentication error > Code: '" . $result["ERROR_CODE"] . "' Text: '" . $result["ERROR_CHARDATA"] . "' Time: '" . $result["ERROR_TIME"] . "' Order_ID: '" . $result["RESPONSE_ORDER_ID"] . "'");
                };
            };
            if (in_array("DOCUMENT", $result)) {

                $this->saveTransaction($result);
                Log::info("result" . $result);

            };
        } else {
            Log::info("System error" . $result);
        };
    }

    public function saveTransaction($result)
    {
        $order = Order::whereId(intval($result["ORDER_ORDER_ID"]))->first();
        $order->status = 1;
        $order->save();

        $transaction = Transaction::where('order_id', $order->id)->first();
        $transaction->is_confirmed = 1;
        $transaction->responce = json_encode($result);
        $transaction->save();

        $path1 = '../app/kkb/config.txt';
        $this->orderForEmail($order);

        $xml = process_complete($result["PAYMENT_REFERENCE"], $result["PAYMENT_APPROVAL_CODE"], intval($result["ORDER_ORDER_ID"]), 398, $result["ORDER_AMOUNT"], $path1);

        if ($curl = curl_init()) {
            curl_setopt($curl, CURLOPT_URL, 'https://epay.kkb.kz/jsp/remote/control.jsp?' . urlencode($xml) . '');
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            $out = curl_exec($curl);
            curl_close($curl);
        }

        return Redirect::route('orderaccepted', ['id' => $order->id]);
    }

    function orderForEmail($order)
    {
        $data = [
            'user_id' => $order->user_id,
            'order' => $order,
            'promocode' => $order->promocode,
        ];

        \Mail::send('email.order', $data, function ($message) use ($order) {
            $message->from('no-reply@plov.kz', 'RUMI - онлайн ресторан')->to(['salem@plov.kz','rumidostavka20@gmail.com'])->subject('RUMI: Новый заказ' . ' #' . $order->id);
        });
    }

    

    public function repeatOrder(Request $request, $id)
    {
        $order = Order::find($id);
        $products = $order->products;
        $arr = [];
        foreach ($products as $key => $product) {


            $item = [];
            $item['id'] = $product->id;
            $item['iiko_id'] = $product->iiko_id;
            $item['quantity'] = $product->pivot->qty;
            $item['title'] = $product->title;
            $item['price'] = $product->price;
            $item['image'] = $product->main_image;
            $item['weight'] = $product->weight;
            $arr[$product->id] = $item;
        }

        return response()->json($arr);
    }

    public function addSingle($id)
    {
        $product = Product::where('id', $id)->first();
        $item['id'] = $product->id;
        $item['iiko_id'] = $product->iiko_id;
        $item['quantity'] = 1;
        $item['title'] = $product->title;
        $item['price'] = $product->price;
        $item['image'] = $product->images->first->url ? $product->images->first->url : '/app/voyager/placeholder.png';
        $item['weight'] = $product->weight;

        return response()->json($item);
    }

    public function accepted($id)
    {
        $order = Order::findOrFail($id);
        return view('page.order-accepted', compact('order'));
    }



}
