<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Session;

class LanguageController extends Controller
{
    //
    public function index(Request $request)
    {
        
        if (in_array($request->get('locale'), \Config::get('app.locales'))) {

            Cookie::queue('locale', $request->get('locale'), 2628000);
        }
        
        return redirect()->back();
    }

    public function currentLang()
    {
        return response()->json(App::getLocale());
    }
}
