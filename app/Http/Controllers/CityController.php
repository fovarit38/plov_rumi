<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CityController extends Controller
{
    //
    
    public function change(Request $request)
    {
        \Cookie::queue('city', $request->city, 2628000);
        return redirect()->back();
    }
}
