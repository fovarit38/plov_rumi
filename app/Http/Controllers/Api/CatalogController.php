<?php

namespace App\Http\Controllers\Api;

use App\Category;
use App\City;
use App\Helpers;
use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Input;

class CatalogController extends Controller
{
    //
    const URL = 'https://iiko.biz:9900/api/0';

    public function getToken($city)
    {
        $token = curl_get(self::URL . '/auth/access_token', array('user_id' => $city->login, 'user_secret' => $city->pass ));
        return trim($token, '"');
    }

    public function getOrganizationList($city)
    {
        $organizations = curl_get(self::URL . '/organization/list', array('access_token' => $this->getToken($city)));
        return $organizations;
    }

    public function getNomenclature($citySlug)
    {

        $city = City::where('slug', $citySlug)->first();
        $organization = array_first(\GuzzleHttp\json_decode($this->getOrganizationList($city)));
        $categories_json = curl_get(self::URL . '/nomenclature/' . $organization->id, array('access_token' => $this->getToken($city)));
        $categories = \GuzzleHttp\json_decode($categories_json)->groups;
        $products = \GuzzleHttp\json_decode($categories_json)->products;

        $this->categoriesSave($categories, $city->slug);
        $this->productsSave($products, $city->slug);

        return response()->json();
    }

    public function getOrderInfo($city)
    {
        $order_id = '52966';
        $organization_id = 'c1b3fbe3-5fd6-11e7-80df-d8d38565926f';
        return curl_get(self::URL . '/orders/info', array(CURLOPT_HTTPHEADER => array('Content-Type: application/json; charset=utf-8'), 'access_token' => $this->getToken($city), 'organization' => $organization_id, 'order' => $order_id));
    }


    public function categoriesSave($categories, $city)
    {
        foreach ($categories as $category) {

            if (Category::where('iiko_id', '=', $category->id)->exists()) {
                $old_cat = Category::where('iiko_id' , $category->id)->first();
                $old_cat->name = $category->name;
                $old_cat->slug = Helpers::str2url( $old_cat->name);
                $old_cat->order = $category->order;
                $old_cat->city = $city;
                $old_cat->save();
            }
            
            else {
                $new_cat = new Category();
                $new_cat->name = $category->name;
                $new_cat->slug = Helpers::str2url($new_cat->name);
                $new_cat->iiko_id = $category->id;
                $new_cat->order = $category->order;
                $new_cat->city = $city;
                $new_cat->save();
            }
        }
    }

    public function productsSave($products , $city)
    {
        foreach ($products as $product) {

            if (Product::where('iiko_id', '=', $product->id)->exists()) {
                $old_product = Product::where('iiko_id' , $product->id)->first();
                $old_product->title = $product->name;
                $old_product->slug = Helpers::str2url( $old_product->title);
                $old_product->price = $product->price;
                $old_product->category_id = $product->parentGroup;
                $old_product->type = $product->type;
                $old_product->city = $city;
                $old_product->order = $product->order;
                $old_product->save();
            }

            else {
                $new_product = new Product();
                $new_product->title = $product->name;
                $new_product->slug = Helpers::str2url($new_product->title);
                $new_product->iiko_id = $product->id;
                $new_product->price = $product->price;
                $new_product->category_id = $product->parentGroup;
                $new_product->type = $product->type;
                $new_product->city = $city;
                $new_product->order = $product->order;
                $new_product->save();
            }
        }
    }

    public function getProducts($id)
    {

        $category = Category::find($id);

        $products = $category->products()->where('city' ,City::getCookieCity())->where('active' , 1)->with('category')->get();
      
        return response()->json($products);
    }

    public function slider(Request $request)
    {
        $products = Product::where('in_slider', 1)->where('active', 1)->get();

        $arr = [];

        foreach ($products as $key => $product) {
            $item = [];
            $item['id'] = $product->id;
            $item['iiko_id'] = $product->iiko_id;
            $item['title'] = $product->title;
            $item['price'] = $product->price;
            $item['image'] = $product->image;
            $item['weight'] = $product->weight;
            $arr[$product->id] = $item;
        }

        return response()->json($arr);
    }

    public function recommend($id)
    {
        $product = Product::find($id);

        $arr = [];
        foreach ($product->recommend as $key => $product) {

            $item = [];
            $item['id'] = $product->id;
            $item['iiko_id'] = $product->iiko_id;
            $item['title'] = $product->title;
            $item['price'] = $product->price;
            $item['image'] = $product->image;
            $item['weight'] = $product->weight;
            $arr[$product->id] = $item;
        }
        return response()->json($arr);
    }

    public function getProductByType(Request $request)
    {
        $product = [];

        if ($request->type == 'simple')
        {
            $product = Product::with('category')->get();
            return response()->json($product);
        }

//        if ($request->type == 'top-left')
//        {
//            $product = Product::where('type' , 'top-left')->with('category')->first();
//            return response()->json($product);
//        }
//
//        if ($request->type == 'top-right')
//        {
//            $product = Product::where('type' , 'top-right')->with('category')->first();
//            return response()->json($product);
//        }
//
//        if ($request->type == 'right-bottom')
//        {
//            $product = Product::where('type' , 'right-bottom')->with('category')->first();
//            return response()->json($product);
//        }
//
        return response()->json($product);
    }


}
