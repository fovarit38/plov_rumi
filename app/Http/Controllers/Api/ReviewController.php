<?php

namespace App\Http\Controllers\Api;

use App\Review;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ReviewController extends Controller
{
    //

    public function getReviews(Request $request, $id)
    {
        $reviews = Review::where('product_id', $id)->where('published', 1)->with('user')->get();
        foreach ($reviews as $review){
            $review->date = \LocalizedCarbon::instance($review->created_at)->diffForHumans();
        }


        return response()->json($reviews);
    }

    public function save(Request $request, $id)
    {
        $review = new Review();
        $review->product_id = $id;
        $review->text = $request->text;
        $review->user_id = \Auth::user()->id;
        $review->rating = $request->rating;
        $review->published = 0;
        $review->save();

        return response()->json('Вы успешно добавили отзыв! Ваш отзыв появится на сайте после модерации');
    }

    public function recent()
    {
        $reviews = Review::where('published', 1)->with('user', 'product')->limit(9)->orderBy('created_at','desc')->get();

        return response()->json($reviews);
    }
}
