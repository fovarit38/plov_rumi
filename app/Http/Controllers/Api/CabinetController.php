<?php

namespace App\Http\Controllers\Api;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

class CabinetController extends Controller
{
    //
    private $newLogoFileName;

    public function user()
    {
        $user = User::where('id' , \Auth::user()->id)->with('orders')->first();
        return response()->json($user);
    }

    public function update(Request $request)
    {
      
        $user = \Auth::user();
        $user->name = $request->name;
        $user->gender = $request->gender;
        $user->born = $request->born;
        $user->phone = $request->phone;
        $user->street = $request->street;
        $user->home = $request->home;
        $user->apartment = $request->apartment;
        $user->floor = $request->floor;
        $user->entrance = $request->entrance;
        $user->doorphone = $request->doorphone;        
        $user->save();

        return response()->json('ok');
    }


    public function uploadAvatar(Request $request)
    {

        $user  = \Auth::user();
        $file = Input::file('file');

        if ($error = $this->validateLogoUpload($file))
            return response()->json(['error' => $error]);

        $logoPath = $this->doUploadAva($user->id, $file);

        $this->deleteOldLogoFile($user->avatar);

        $user->avatar = $logoPath . $this->newLogoFileName;
        $user->save();

        return response()->json(['url' => $logoPath . $this->newLogoFileName, 'message' => 'Вы успешно загрузили логотип']);
    }

    protected function deleteOldLogoFile($oldEntry)
    {

        if ($oldEntry)
            \File::delete(trim($oldEntry, '/'));
    }

    protected function validateLogoUpload($file)
    {
        $validator = \Validator::make(['image' => $file], ['image' => 'mimes:jpeg,jpg,png|required']);

        if ($validator->fails()) {
            return 'Not an image.';
        }

        return false;
    }

    protected function doUploadAva($userId, $file)
    {
        $logoPath = 'app/voyager/users/' . $userId . '/';
        $destinationPath = public_path() . '/' . $logoPath;
        $file->move($destinationPath, $this->getNewLogoFileName());

        return $logoPath;
    }


    protected function getNewLogoFileName()
    {
        if (!$this->newLogoFileName)
            $this->newLogoFileName = str_random(10) . '.jpg';

        return $this->newLogoFileName;
    }
}