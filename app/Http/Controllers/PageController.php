<?php

namespace App\Http\Controllers;

use App\City;
use App\Helpers;
use App\Order;
use App\Product;
use App\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use TCG\Voyager\Models\Page;
use Dosarkz\EPayKazCom\Facades\Epay;
use Illuminate\Support\Facades\Redirect;

class PageController extends Controller
{
    public function delivery()
    {
        return view('page.delivery');
    }

    public function checkout()
    {

        return view('page.checkout');
    }

    public function about()
    {
        $page = Page::where('slug', 'about')->first();
        return view('page.static', compact('page'));
    }

    public function contacts()
    {
        $page = Page::where('slug', 'contacts')->first();
        return view('page.static', compact('page'));
    }

    public function test()
    {

       
        $products = Product::where('id', 3)->get();

        $order = new Order();
        $order->name = 'test';
        $order->phone = '+77471118083';
        $order->email = 'aubakir.adilbek@gmail.com';
        $order->amount = 10;
        $order->delivery_amount = 0;
        $order->delivery = 0;
        $order->total_amount = 10;
        $order->street = 'Abaya';
        $order->home = 1;
        $order->apartment = 1;
        $order->entrance = 1;
        $order->floor = 1;
        $order->doorphone = 1;
        $order->payment = 'online';
        $order->zdacha = '';
        $order->square = 1;
        $order->time = '';
        $order->status = false;
        $order->city = 'almaty';
        $order->save();

        foreach ($products as $product) {
            $order->products()->attach($product['id'], ['qty' => 1]);
        }

        if ($order->payment == 'online') {

            $transaction = new Transaction();
            $transaction->order_id = $order->id;
            $transaction->amount = $order->total_amount;
            $transaction->is_confirmed = false;
            $transaction->save();

            $pay = Epay::basicAuth([
                'order_id' => $order->id,
                'currency' => '398',
                'amount' => $order->total_amount,
                'email' => $order->email,
                'hashed' => true,
            ]);

            if ($pay->generateUrl()) {
                return Redirect::to($pay->generateUrl());
            }
            return false;
        }
        $this->orderForEmail($order);

        return view('page.test');
    }

    function orderForEmail($order)
    {
        $data = [
            'user_id' => $order->user_id,
            'order' => $order,
            'promocode' => $order->promocode,
        ];

        \Mail::send('email.order', $data, function ($message) use ($order) {
            $message->from('no-reply@plov.kz', 'RUMI - онлайн ресторан')->to(['salem@plov.kz','rumidostavka20@gmail.com','aubakir.adilbek@gmail.com'])->subject('RUMI: Новый заказ' . ' #' . $order->id);
        });
    }

}
