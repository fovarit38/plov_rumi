<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;

class CabinetController extends Controller
{
    //

    public function history()
    {
        $user = \Auth::user();
        return view('cabinet.profile', compact('user'));
    }

    public function contacts()
    {
        $user = \Auth::user();
        return view('cabinet.contacts', compact('user'));
    }

    public function settings()
    {
        $user = \Auth::user();
        return view('cabinet.settings', compact('user'));
    }
}
