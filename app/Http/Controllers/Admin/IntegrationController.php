<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IntegrationController extends Controller
{

    public function index()
    {
        return view('vendor.voyager.integration.browse');
    }
}
