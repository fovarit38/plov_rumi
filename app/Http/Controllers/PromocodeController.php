<?php

namespace App\Http\Controllers;

use App\City;
use App\Promocode;
use Carbon\Carbon;
use Illuminate\Http\Request;

class PromocodeController extends Controller
{
    //

    public function getPromocode(Request $request)
    {

        $this->validate($request, [
            'code' => 'required|string'
        ]);

        $promocode = Promocode::where('code', $request->code)->where('city', City::getCookieCity())->where('end_date', '>=', Carbon::now())->first();

        if (!$promocode) {
            return response()->json('Промокод не найден', 400);
        }

        return ['promocode' => $promocode];
    }
}
