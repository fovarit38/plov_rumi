<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Response;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Models\Page;

class SiteDisable
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if (Voyager::setting('site_disable')) {
            $page = Page::findOrFail(4);
            return Response::make(view('page.disable-page', compact('page')), 404);
        }

        return $next($request);
    }
}
