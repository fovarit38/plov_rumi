<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cookie;

class City extends Model
{
    //


    public static function getCookieCity()
    {

        if (\Cookie::has('city')) {
            return Cookie::get('city');
        }
        else{
            return 'almaty';
        }
    }
}
