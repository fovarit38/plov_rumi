<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
    //

    protected $casts = [
        'active' => 'boolean'
    ];

    public function getImageUrlAttribute()
    {
        $image = Image::where('object_id' , $this->id)->where('object_type', 'slider')->first();
        return $image ? $image->url : 'placeholder.png';
    }
}
