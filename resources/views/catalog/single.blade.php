@extends('layouts.page')

@section('content')

    <div class="clearfix"></div>
    <div class="product-single" ng-controller="CatalogController as cg">
        <div class="container">

            <div class="row no-padding">
                <div class="col-lg-12 hidden-xs">
                    <div class="breadcrumbs">
                        <ul>
                            <li><a href="/"> @lang('product.rumi_name') </a></li>
                            <li><a href="/catalog/{{ $product->category->slug }}">{{ $product->category->name }}</a></li>
                            <li>{{ $product->title }}</li>
                        </ul>
                    </div>
                </div>
                <div class="clearfix"></div>

                <div class="col-lg-7">
                    <div class="slider">
                        <section class="slider-product" >

                            @foreach($product->images()->where('object_id' , $product->id)->get() as $image)
                                <div data-thumb="{{$image->url}}"><img src="{{$image->url}}"></div>
                            @endforeach
                        </section>

                    </div>
                </div>
                <div class="col-lg-5">
                    <div class="right-block">
                        <div class="product">
                            <div class="title">{{ $product->title }}</div>
                            <div class="review-block">
                                <span class="rating-star-wrap">{{ $product->rating_stars }}</span>
                                <div class="review-count"><a href="#reviews">{{ $product->reviews()->count() }} {{ \App\Helpers::review_str($product->reviews()->count()) }}</a></div>

                            </div>
                            <div class="text">
                                {{ $product->text }}
                            </div>

                            <div class="text composition">
                                <span>Состав:</span>
                                {{ $product->composition }}
                            </div>

                           <div class="footer">
                               <div class="col-lg-6 col-xs-6">
                                   <div class="price"> {{ $product->price }} ₸</div>
                                   <div class="count">
                                       <span> 1 порция </span>
                                       <span> {{ $product->weight }}</span>
                                   </div>
                               </div>
                               <div class="col-lg-6 col-xs-6">
                                   <div class="add-to-cart" ng-class="{flip: ct.cart[ct.singleProduct.id].quantity == 0 || ct.cart[ct.singleProduct.id].quantity != undefined}">
                                       <div class="btn-wrap front" >
                                           <a href="#" class="btn btn-orange" ng-click="ct.addToCart(ct.singleProduct);$event.preventDefault()">
                                               Заказать
                                           </a>
                                       </div>
                                       <div class="quantity-control back" ng-init="ct.addSingleProduct('{{$product->id}}')">
                                           <ul>
                                               <li class="control-btn minus" ng-click="ct.minus(ct.singleProduct)"></li>
                                               <li class="control-qty" ng-bind="ct.cart[ct.singleProduct.id].quantity || 0"></li>
                                               <li class="control-btn plus" ng-click="ct.addToCart(ct.singleProduct);$event.preventDefault()"></li>
                                           </ul>
                                       </div>
                                   </div>
                               </div>
                           </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="recommended">
            <div class="container">
                <div class="row no-padding">
                    <div class="col-lg-12">
                        <div class="title-wrap">
                            {{--<div class="title">К "{{$product->title}}", мы рекомендуем:</div>--}}

                            <div class="title">@lang('product.recommend_title' , ['product'=> $product->title]):</div>
                        </div>
                    </div>
                    <div class="clearfix"></div>

                    <div class="products-list-index">
                        <div class="recommendslider" ng-init="cg.recommendedProducts('{{$product->id}}')">
                            @foreach($product->recommend as $product)
                                <div class="item product">
                                    <a href="/catalog/{{$product->category->slug}}/{{$product->slug}}">
                                        <div class="image"><img src="{{ $product->main_image }}" alt=""></div>
                                        <div class="title"><span>{{ $product->title }}</span></div>
                                    </a>
                                    <div class="description">
                                        <div class="footer">
                                            <div class="">
                                                <div class="col-lg-6 col-xs-6">
                                                    <div class="price"> {{ $product->price }} ₸</div>
                                                    <div class="count">
                                                        <span>1 порция </span>
                                                        <span>450 гр.</span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-xs-6">

                                                    <div class="add-to-cart" ng-class="{flip: ct.cart['{{$product->id}}'].quantity == 0 || ct.cart['{{$product->id}}'].quantity != undefined}">
                                                        <div class="btn-wrap front">
                                                            <a href="#" class="btn-orange" ng-click="ct.addToCart(cg.recommend['{{$product->id}}']);$event.preventDefault()">
                                                                Заказать
                                                            </a>
                                                        </div>

                                                        <div class="quantity-control back">
                                                            <ul>
                                                                <li class="control-btn minus" ng-click="ct.minus(cg.recommend['{{$product->id}}'])"></li>
                                                                <li class="control-qty" ng-bind="ct.cart['{{$product->id}}'].quantity || 0"></li>
                                                                <li class="control-btn plus" ng-click="ct.addToCart(cg.recommend['{{$product->id}}']);$event.preventDefault()"></li>
                                                            </ul>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach

                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div id="reviews" class="reviews-block" ng-controller="ReviewsController as rv" ng-init="rv.getReviews('{{$product->id}}')">
            <div class="container">
                <div class="row no-padding">
                    <div class="col-lg-12">
                        <div class="title-wrap">
                            <div class="title"> @lang('product.user_reviews')</div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-lg-5">
                        <div class="wrap-form">
                            @if (Auth::guest())
                                <div class="authorized">
                                    <div class="title"> @lang('product.enter_reviews')</div>
                                    <div class="caption">
                                        @lang('product.logged')
                                    </div>
                                    <div class="btn-wrap">
                                        <a href="#" class="btn btn-blue"> @lang('product.login_register') </a>
                                    </div>
                                </div>
                            @else
                                <div class="noauthorized">
                                    <form ng-submit="rv.save('{{$product->id}}')">
                                        <div class="title"> @lang('product.enter_reviews')</div>
                                        <div class="text">
                                        <textarea ng-model="rv.textReview" name="text" class="input-std" id="" cols="10"
                                                  rows="3"
                                                  placeholder="Текст отзыва ..." required></textarea>
                                        </div>
                                        <div class="rating">
                                            <span>Ваша оценка: </span>
                                            <fieldset>
                                            <span class="star-cb-group stars-wrap">
                                                <i ng-repeat="n in [].constructor(5) track by $index"
                                                   ng-click="rv.rating = $index + 1;"
                                                   ng-class="($index + 1) <= rv.rating ?  'rating_star' : 'rating_star_empty'"
                                                   class="rating-star"></i>
                                             </span>
                                            </fieldset>
                                        </div>
                                        <div class="btn-wrap">
                                            <input type="submit" class="btn btn-blue" value="Опубликовать">
                                        </div>
                                    </form>
                                </div>
                            @endif
                        </div>
                    </div>
                    <div class="col-lg-6 col-lg-offset-1">
                        <div class="reviews-list">

                            <div class="review" ng-repeat="review in rv.reviews">
                                <div class="col-lg-6">
                                    <div class="user-info">
                                        <div class="user-image">
                                            <img src="/@{{ review.user.avatar }}" alt=""></div>
                                        <div class="user-name">
                                            <div class="star">
                                                 <span class="rating-star-wrap">
                                                    <i ng-repeat="n in [].constructor(5) track by $index"
                                                       ng-class="($index + 1) <= review.rating ? 'rating_star' : 'rating_star_empty'"></i>
                                                </span>
                                            </div>
                                            <div class="name">@{{ review.user.name }}</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="date">
                                        @{{ review.date }}
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="text">
                                    @{{ review.text }}
                                </div>
                            </div>

                            <div class="review" ng-if="rv.reviews.length == 0">
                                <h4> @lang('product.first_reviews')</h4>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection


