@extends('layouts.page')
@section('content')

    
    <div class="category-page" ng-controller="CatalogController as cg">
        <div class="container">
            <div class="row no-padding">

                <div class="clearfix"></div>
                <div class="title-wrap">
                    <div class="col-lg-12">
                        <div class="title title-category"> {{ $category->name }} </div>
                    </div>
                    <div class="title-mobile">
                        <div class="col-lg-12">
                            <div class="title"> Меню: <span> {{ $category->name }} </span></div>
                        </div>
                    </div>
                </div>

                @if($category->slug == 'sety')
                    <div class="alert alert-info" style="font-size: 16px;">
                        Акция на сеты не действует в выходные и праздничные дни.
                    </div>
                @endif

                <div class="content">
                    <div class="products-list-index" ng-init="cg.products('{{$category->id}}')">
                        <div class="product simple col-lg-12" ng-repeat="product in cg.categoryProducts">
                            <div class="top">
                                <a href="/catalog/@{{ product.category.slug }}/@{{ product.slug }}">
                                    <div class="image">
                                        <img src="@{{ product.main_image }}" alt="">
                                    </div>
                                    <div class="title"><span ng-bind="product.title"></span></div>
                                </a>
                            </div>
                            <div class="clearfix"></div>
                            <div class="description">

                                <div class="text" ng-bind="product.text"></div>
                                <div class="footer row">

                                    <div class="col-lg-6 col-xs-6">
                                        <div class="price"> @{{ product.price }} ₸</div>
                                        <div class="count">
                                            <span>1 порция </span>
                                            <span> @{{ product.weight }}</span>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-xs-6">
                                        <div class="add-to-cart"
                                             ng-class="{flip: ct.cart[product.id].quantity == 0 || ct.cart[product.id].quantity != undefined}">
                                            <div class="btn-wrap front">
                                                <a href="#" class="btn btn-orange"
                                                   ng-click="ct.addToCart(product);$event.preventDefault()">
                                                    Заказать
                                                </a>
                                            </div>
                                            <div class="quantity-control back">
                                                <ul>
                                                    <li class="control-btn minus" ng-click="ct.minus(product)"></li>
                                                    <li class="control-qty">@{{ ct.cart[product.id].quantity || 0}}</li>
                                                    <li class="control-btn plus"
                                                        ng-click="ct.addToCart(product);$event.preventDefault()"></li>
                                                </ul>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>


@endsection