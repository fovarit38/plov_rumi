@extends('layouts.page')

@section('content')
<div class="container">
    <div class="row no-padding">
        <div class="col-lg-12">
            <div class="title-page"> Сброс пароля </div>
            <div class="panel-body">
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif

                <form class="form-horizontal" role="form" method="POST" action="{{ route('password.email') }}">
                    {{ csrf_field() }}

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <label for="email" class="control-label"></label>

                        <div class="col-md-4">
                            <input id="email" type="email" class="input-std" placeholder="E-Mail адрес" name="email" value="{{ old('email') }}" required>

                            @if ($errors->has('email'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-4">
                            <button type="submit" class="btn-blue">Отправить ссылку на сброс пароля</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
