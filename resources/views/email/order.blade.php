<html>
<head>
    <meta charset="utf-8">
</head>
<body>


<h2> Заказ # {{ $order->id }}</h2>

<h4> Информация о клиенте </h4>
<table>
    <tr>
        <td> Имя:</td>
        <td> {{ $order->name}}</td>
    </tr>
    <tr>
        <td> Телефон:</td>
        <td> {{ $order->phone }}</td>
    </tr>
    <tr>
        <td> Тип доставки:</td>
        <td>
            @if($order->delivery)
                Доставка курьером , квадрат: {{ \App\Helpers::$square[$order->square] }}
            @else
                Самовывоз
            @endif

        </td>
    </tr>
    <tr>
        <td> Город:</td>
        <td> {{ \App\City::where('slug', \App\City::getCookieCity())->first()->name }}</td>
    </tr>

    @if($order->delivery)
        <tr>
            <td> Улица:</td>
            <td> {{ $order->street }}</td>
        </tr>
        <tr>
            <td> Дом:</td>
            <td> {{ $order->home }}</td>
        </tr>
        <tr>
            <td> Квартира:</td>
            <td> {{ $order->apartment }}</td>
        </tr>
        <tr>
            <td> Подъезд:</td>
            <td> {{ $order->entrance}}</td>
        </tr>
        <tr>
            <td> Этаж:</td>
            <td> {{ $order->floor }}</td>
        </tr>
        <tr>
            <td> Домофон:</td>
            <td> {{ $order->doorphone }}</td>
        </tr>
        <tr>
            <td> Время доставки:</td>
            <td> {{ $order->time }}</td>
        </tr>
    @endif
    <tr>
        <td> Комментарий:</td>
        <td> {{ $order->zdacha }}</td>
    </tr>
<!-- <tr>
        <td> Нужна cдача с:</td>
        <td> {{ $order->zdacha }}</td>
    </tr> -->
    <tr>
        <td> Тип оплаты:</td>
        @if($order->payment == 'in-cash')
            <td> Оплата наличными</td>
        @endif
        @if($order->payment == 'online')
            <td class=""> Оплата онлайн</td>
        @endif

    </tr>
</table>

<hr/>

<h4> Информация о заказе </h4>
<table border="1" style="border-spacing: 0; border: 1px solid #000000">
    <tr style="padding: 5px; border: 1px solid #000000">
        <th style="padding: 5px; border: 1px solid #000000"> №</th>
        <th style="padding: 5px; border: 1px solid #000000"> Название</th>
        <th style="padding: 5px; border: 1px solid #000000"> Кол-во</th>
        <th style="padding: 5px; border: 1px solid #000000"> Цена</th>
        <th style="padding: 5px; border: 1px solid #000000"> Сумма</th>
    </tr>

    @foreach($order->products as $key => $product)
        <tr style="padding: 5px; border: 1px solid #000000">
            <td style="padding: 5px; border: 1px solid #000000"> {{ $key + 1 }} </td>
            <td style="padding: 5px; border: 1px solid #000000"> {{ $product->title }} </td>
            <td style="padding: 5px; border: 1px solid #000000"> {{ $product->pivot->qty }} </td>
            <td style="padding: 5px; border: 1px solid #000000"> {{ $product->price }} тг</td>
            <td style="padding: 5px; border: 1px solid #000000"> {{ $product->price * $product->pivot->qty }} тг</td>
        </tr>
    @endforeach

    @if($promocode)
        @if($promocode->type == 'product')
            <tr style="padding: 5px; border: 1px solid #000000">
                <td style="padding: 5px; border: 1px solid #000000"> {{$order->products->count() + 1 }} </td>
                <td style="padding: 5px; border: 1px solid #000000"> {{ $promocode->product->title }} </td>
                <td style="padding: 5px; border: 1px solid #000000"> 1 </td>
                <td style="padding: 5px; border: 1px solid #000000"> 0 тг</td>
                <td style="padding: 5px; border: 1px solid #000000"> 0 тг</td>
            </tr>
        @endif
    @endif

    @if($order->delivery)
        <tr style="padding: 5px;border: 1px solid #000000">
            <td colspan="4" style="border: 1px solid #000000"> Сумма доставки:</td>
            <td style="border: 1px solid #000000"> {{ $order->square == 1 && $order->amount >= 10000 ? 0 : \App\Helpers::$squarePrice[$order->square] }}
                тг
            </td>
        </tr>

    @endif

    <tr style="padding: 5px;border: 1px solid #000000">
        <td colspan="4" style="border: 1px solid #000000">
            Сумма:
        </td>
        <td style="border: 1px solid #000000">
            {{ $order->amount + $order->delivery_amount }} тг
        </td>
    </tr>


    @if($promocode)


        @if($promocode->type == 'discount')
            <tr style="padding: 5px;border: 1px solid #000000">
                <td colspan="4" style="border: 1px solid #DD002F">Скидка по промокоду</td>
                <td style="border: 1px solid #DD002F"> - {{ $promocode->discount }}%</td>
            </tr>
            <tr style="padding: 5px;border: 1px solid #000000">
                <td colspan="5" style="border: 1px solid #DD002F">Промокод: {{$promocode->title}} </td>
            </tr>
            <tr style="padding: 5px;border: 1px solid #DD002F">
                <td colspan="4" style="border: 1px solid #DD002F">
                    Итоговая сумма:
                </td>
                <td style="border: 1px solid #DD002F">
                    {{ $order->total_amount }} тг
                </td>
            </tr>
        @endif

        @if($promocode->type == 'sum')
            <tr style="padding: 5px;border: 2px solid #DD002F">
                <td colspan="4" style="border: 2px solid #DD002F">Скидка</td>
                <td style="border: 2px solid #DD002F"> - {{ $promocode->sum}} тг</td>
            </tr>
                <tr style="padding: 5px;border: 2px solid #DD002F">
                <td colspan="5" style="border: 2px solid #DD002F">Промокод: {{$promocode->title}} </td>

            </tr>
            <tr style="padding: 5px;border: 1px solid #DD002F">
                <td colspan="4" style="border: 2px solid #DD002F">
                    Итоговая сумма:
                </td>
                <td style="border: 2px solid #DD002F">
                     {{ $order->total_amount }} тг
                </td>
            </tr>
        @endif

        @if($promocode->type == 'delivery')
            <tr style="padding: 5px;border: 1px solid #dd002f">
                <td colspan="5" style="border: 2px solid #DD002F"> Промокод: {{$promocode->title}} </td>
            </tr>
                <tr style="padding: 5px;border: 1px solid #dd002f">
                <td colspan="4" style="border: 2px solid #DD002F"> Доставка</td>
                <td style="border: 1px solid #DD002F"> бесплатно</td>
            </tr>
            <tr style="padding: 5px;border: 2px solid #DD002F">
                <td colspan="4" style="border: 2px solid #DD002F">
                    Итоговая сумма:
                </td>
                <td style="border: 2px solid #DD002F">
                    {{ $order->total_amount }} тг
                </td>
            </tr>
        @endif

            @if($promocode->type == 'product')
                <tr style="padding: 5px;border: 2px solid #dd002f">
                    <td colspan="5" style="border: 2px solid #DD002F">Промокод: {{$promocode->title}}   </td>
                </tr>
                <tr style="padding: 5px;border: 2px solid #DD002F">
                    <td colspan="4" style="border: 2px solid #DD002F">
                        Итоговая сумма:
                    </td>
                    <td style="border: 2px solid #DD002F">
                        {{ $order->total_amount }} тг
                    </td>
                </tr>
            @endif

    @endif


</table>


</body>
</html>

