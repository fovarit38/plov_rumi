@extends('voyager::master')

@if(isset($dataTypeContent->id))
    @section('page_title','Редактировать блюда')
@else
    @section('page_title','Добавить блюда')
@endif

@section('css')
    <style>
        .gallery-images {
            margin-bottom: 40px;
        }

        .gallery-images .image {
            display: inline-block;
            position: relative;
            width: 200px;
            margin-right: 10px;
        }

        .gallery-images .image img {
            width: 100%;
        }

        .gallery-images .image .delete {
            display: inline-block;
            width: 22px;
            height: 22px;
            background: red;
            color: #ffffff;
            text-align: center;
            padding: 0 3px;
            border-radius: 15px;
            position: absolute;
            top: -7px;
            right: -7px;
            font-weight: bold;
        }

    </style>
@stop

@section('page_header')
    <h1 class="page-title">
        <i class="{{ $dataType->icon }}"></i> @if(isset($dataTypeContent->id)){{ 'Редактировать блюда' }}@else{{ 'Новая блюда' }}@endif {{ ''}}
    </h1>
@stop

@section('content')
    <div class="page-content container-fluid" ng-app="adminRumi" ng-controller="GalleryController as gl">

        <link rel="stylesheet" href="/js/angular-loading-bar/build/loading-bar.min.css" type='text/css' media='all'/>
        <script src="/js/angular/angular.min.js"></script>
        <script src="/js/admin/app.module.js"></script>
        <script src="/js/admin/gallery.controller.js"></script>
        <script src="/js/ng-file-upload/ng-file-upload-shim.min.js"></script>
        <script src="/js/ng-file-upload/ng-file-upload.min.js"></script>
        <script type='text/javascript' src='/js/angular-loading-bar/build/loading-bar.min.js'></script>
        <script src="/js/slugify/speakingurl.js"></script>
        <script src="/js/slugify/slugify.min.js"></script>
        <script src="/js/angular-notify/dist/angular-notify.min.js"></script>

        <form role="form"
              action="@if(isset($dataTypeContent->id)){{ route('voyager.products.update', $dataTypeContent->id) }}@else{{ route('voyager.products.store') }}@endif"
              method="POST" enctype="multipart/form-data">
            <!-- PUT Method if we are editing -->
            @if(isset($dataTypeContent->id))
                {{ method_field("PUT") }}
            @endif

            {{ csrf_field() }}

            <div class="row">

                <div class="col-md-12">
                    <!-- ### TITLE ### -->
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title"> Название </h3>
                        </div>
                        <div class="panel-body">
                            <input type="text" id="title" class="form-control" name="title"
                                   placeholder="Назввание на русском"
                                   value="@if(isset($dataTypeContent->title)){{ $dataTypeContent->title }}@endif">
                        </div>
                        <div class="panel-body">
                            <input type="text" id="title_kz" class="form-control" name="title_kz"
                                   placeholder="Назввание на казахском"
                                   value="@if(isset($dataTypeContent->title_kz)){{ $dataTypeContent->title_kz }}@endif">
                        </div>

                        <div class="panel-body">
                            <input type="text" id="title_en" class="form-control" name="title_en"
                                   placeholder="Назввание на англ"
                                   value="@if(isset($dataTypeContent->title_en)){{ $dataTypeContent->title_en }}@endif">
                        </div>
                    </div>
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title"> Рекомендуемые блюда</h3>
                        </div>
                        <div class="panel-body">
                            <select class="js-example-responsive form-control" name="recommend[]" id="recommend"
                                    multiple="multiple">
                                @foreach(\App\Category::where('city', $dataTypeContent->city)->get() as $category)
                                    <optgroup label="{{ $category->name }}">
                                        @foreach($category->products as $product)
                                            <option value="{{ $product->id }}" @if(isset($dataTypeContent->iiko_id)) {{ \App\Product::find($dataTypeContent->id)->recommend->contains($product->id) ? 'selected' : '' }} @endif> {{ $product->title }}</option>
                                        @endforeach
                                    </optgroup>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title"> Ссылка </h3>
                        </div>

                        <div class="panel-body">
                            <input type="text" class="form-control" id="slug" name="slug" placeholder="slug"
                                   value="@if(isset($dataTypeContent->slug)){{ $dataTypeContent->slug }}@endif">
                        </div>
                    </div>
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title"> Цена </h3>
                        </div>

                        <div class="panel-body">
                            <input type="text" class="form-control" id="price" name="price" placeholder="price"
                                   value="@if(isset($dataTypeContent->price)){{ $dataTypeContent->price }}@endif">
                        </div>
                    </div>
                    <div class="panel">

                        <div class="panel-heading">
                            <h3 class="panel-title"> Категория </h3>
                        </div>

                        <div class="form-group">
                            @if($dataTypeContent->category)
                                <select class="form-control" id="category_id" name="category_id">
                                    <optgroup label="Алматы">
                                        @foreach(\App\Category::where('city','almaty')->orderBy('name')->get() as $category)
                                            <option value="{{ $category->iiko_id }}" @if(isset($dataTypeContent->id) && $dataTypeContent->category->iiko_id == $category->iiko_id){{ 'selected' }}@endif >{{$category->name }}</option>
                                        @endforeach
                                    </optgroup>

                                    <optgroup label="Астана">
                                        @foreach(\App\Category::where('city','astana')->orderBy('name')->get() as $category)
                                            <option value="{{ $category->iiko_id }}" @if(isset($dataTypeContent->id) && $dataTypeContent->category->iiko_id == $category->iiko_id){{ 'selected' }}@endif >{{$category->name }}</option>
                                        @endforeach
                                    </optgroup>

                                </select>
                            @else
                                <select class="form-control" id="category_id" name="category_id">
                                    <option value=""> ====== Выберите категорию ======</option>

                                    <optgroup label="Алматы">
                                        @foreach(\App\Category::where('city','almaty')->orderBy('name')->get() as $category)
                                            <option value="{{ $category->iiko_id }}">{{$category->name }}</option>
                                        @endforeach
                                    </optgroup>

                                    <optgroup label="Астана">
                                        @foreach(\App\Category::where('city','astana')->orderBy('name')->get() as $category)
                                            <option value="{{ $category->iiko_id }}"  >{{$category->name }}</option>
                                        @endforeach
                                    </optgroup>
                                </select>
                            @endif
                        </div>
                    </div>
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title"> Город </h3>
                        </div>
                        <div class="panel-group">
                            @if($dataTypeContent->city)
                                <select class="form-control" id="city" name="city">
                                    @foreach(\App\City::all() as $city)
                                        <option value="{{ $city->slug }}" @if(isset($dataTypeContent->id) && $dataTypeContent->city == $city->slug){{ 'selected' }}@endif>{{$city->name }}</option>
                                    @endforeach
                                </select>

                            @else
                                <select class="form-control" id="city" name="city">
                                    <option value=""> ====== Выберите город ======</option>
                                    @foreach(\App\City::all() as $city)
                                        <option value="{{ $city->slug }}">{{$city->name }}</option>
                                    @endforeach
                                </select>
                            @endif
                        </div>
                    </div>


                </div>

                <div class="col-md-12">
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title"> Размер </h3>
                        </div>
                        <div class="panel-body">
                            <input type="text" class="form-control" name="weight" placeholder="1 порция/гр"
                                   value="@if(isset($dataTypeContent->weight)){{ $dataTypeContent->weight }}@endif">
                        </div>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title"></h3>
                        </div>
                        <div class="panel-body">
                            <input type="hidden" class="form-control" name="iiko_id" placeholder=""
                                   value="@if(isset($dataTypeContent->iiko_id)){{ $dataTypeContent->iiko_id }}@endif">
                        </div>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title"> Описание </h3>
                        </div>
                        <div class="panel-body">
                            <textarea class="form-control" name="text" cols="30" rows="5"
                                      placeholder="описание на русском">@if(isset($dataTypeContent->text)){{$dataTypeContent->text}}@endif</textarea>
                        </div>
                        <div class="panel-body">
                            <textarea class="form-control" name="text_kz" cols="30" rows="5"
                                      placeholder="описание на казахском">@if(isset($dataTypeContent->text_kz)){{$dataTypeContent->text_kz}}@endif</textarea>
                        </div>

                        <div class="panel-body">
                            <textarea class="form-control" name="text_kz" cols="30" rows="5"
                                      placeholder="описание на англ">@if(isset($dataTypeContent->text_en)){{$dataTypeContent->text_en}}@endif</textarea>
                        </div>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title"> Состав </h3>
                        </div>
                        <div class="panel-body">
                            <textarea class="form-control" name="composition" cols="30" rows="5"
                                      placeholder="состав на русском">@if (isset($dataTypeContent->composition)){{ $dataTypeContent->composition }}@endif</textarea>
                        </div>
                        <div class="panel-body">
                            <textarea class="form-control" name="composition_kz" cols="30" rows="5"
                                      placeholder="состав на казахском">@if (isset($dataTypeContent->composition_kz)){{$dataTypeContent->composition_kz}}@endif</textarea>
                        </div>

                        <div class="panel-body">
                            <textarea class="form-control" name="composition_en" cols="30" rows="5"
                                      placeholder="состав на англ">@if (isset($dataTypeContent->composition_en)){{$dataTypeContent->composition_en}}@endif</textarea>
                        </div>
                    </div>
                </div>


                <div class="col-md-12">
                    <div class="panel-heading">
                        <h3 class="panel-title"> Галерея </h3>
                    </div>
                    <div class="panel">
                        @if(isset($dataTypeContent->id))
                            <div ng-init="gl.getImages('product', '{{$dataTypeContent->id}}')">

                                <div class="gallery-images">
                                    <div class="image" ng-repeat="image in gl.images">
                                        <img ng-src="@{{ image.url }}" alt="">
                                        <div class="delete"
                                             ng-click="gl.deleteImage(image, '{{$dataTypeContent->id}}', 'product')">x
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <!-- ### IMAGES ### -->
                            <div class="add-more btn btn-primary"
                                 ngf-select="gl.uploadImages($files , {{$dataTypeContent->id}},'product')"
                                 accept="image/*" multiple>Добавить изображении
                            </div>

                        @else<?='<i class="icon wb-plus-circle"></i>  Создайте блюда'; ?> @endif
                    </div>
                </div>

                <div class="col-md-12">

                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title"> Добавить в слайдер на глав. стр </h3>
                        </div>
                        <div class="panel-body">
                            @if($dataTypeContent->in_slider == 1)
                                <input type="checkbox" name="in_slider" placeholder="" value="1" checked>
                            @else
                                <input type="checkbox" name="in_slider" placeholder="" value="0">
                            @endif


                        </div>
                    </div>
                </div>
                <div class="col-md-12">

                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title"> Добавить в популярные </h3>
                        </div>
                        <div class="panel-body">
                            @if($dataTypeContent->in_popular == 1)
                                <input type="checkbox" name="in_popular" placeholder="" value="1" checked>
                            @else
                                <input type="checkbox" name="in_popular" placeholder="" value="0">
                            @endif


                        </div>
                    </div>
                </div>
                <div class="col-md-12">

                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title"> Скрыть/Показать </h3>
                        </div>
                        <div class="panel-body">
                            @if($dataTypeContent->active == 1)
                                <input type="checkbox" name="active" placeholder="" value="1" checked>
                            @else
                                <input type="checkbox" name="active" placeholder="" value="0">
                            @endif


                        </div>
                    </div>
                </div>
                <div class="form-group ">


                </div>


            </div>

            <button type="submit" class="btn btn-primary pull-right">
                @if(isset($dataTypeContent->id)){{ 'Обновить' }}@else<?= '<i class="icon wb-plus-circle"></i> Создать'; ?>@endif
            </button>
        </form>

        <iframe id="form_target" name="form_target" style="display:none"></iframe>
        <form id="my_form" action="{{ route('voyager.upload') }}" target="form_target" method="post"
              enctype="multipart/form-data" style="width:0px;height:0;overflow:hidden">
            {{ csrf_field() }}
            <input name="image" id="upload_file" type="file" onchange="$('#my_form').submit();this.value='';">
            <input type="hidden" name="type_slug" id="type_slug" value="{{ $dataType->slug }}">
        </form>
    </div>
@stop

@section('javascript')

    <script src="{{ config('voyager.assets_path') }}/lib/js/tinymce/tinymce.min.js"></script>
    <script src="{{ config('voyager.assets_path') }}/js/voyager_tinymce.js"></script>


    <script>
        jQuery(function ($) {
            $.slugify();
            $('#slug').slugify('#title');
        });


        $('document').ready(function () {
            $('.toggleswitch').bootstrapToggle();
        })
    </script>
    <script type="text/javascript">
        $('#recommend').select2({});
    </script>


@stop