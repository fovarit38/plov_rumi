@extends('voyager::master')

@if(isset($dataTypeContent->id))
    @section('page_title','Редактировать блюда')
@else
    @section('page_title','Добавить блюда')
@endif

@section('css')
    <style>
        .gallery-images {
            margin-bottom: 40px;
        }

        .gallery-images .image {
            display: inline-block;
            position: relative;
            width: 200px;
            margin-right: 10px;
        }

        .gallery-images .image img {
            width: 100%;
        }

        .gallery-images .image .delete {
            display: inline-block;
            width: 22px;
            height: 22px;
            background: red;
            color: #ffffff;
            text-align: center;
            padding: 0 3px;
            border-radius: 15px;
            position: absolute;
            top: -7px;
            right: -7px;
            font-weight: bold;
        }

    </style>
@stop

@section('page_header')
    <h1 class="page-title">
        <i class="{{ $dataType->icon }}"></i> @if(isset($dataTypeContent->id)){{ 'Редактировать баннер' }}@else Новый
        баннер @endif {{ ''}}
    </h1>
@stop

@section('content')
    <div class="page-content container-fluid" ng-app="adminRumi" ng-controller="GalleryController as gl">

        <link rel="stylesheet" href="/js/angular-loading-bar/build/loading-bar.min.css" type='text/css' media='all'/>
        <script src="/js/angular/angular.min.js"></script>
        <script src="/js/admin/app.module.js"></script>
        <script src="/js/admin/gallery.controller.js"></script>
        <script src="/js/ng-file-upload/ng-file-upload-shim.min.js"></script>
        <script src="/js/ng-file-upload/ng-file-upload.min.js"></script>
        <script type='text/javascript' src='/js/angular-loading-bar/build/loading-bar.min.js'></script>
        <script src="/js/slugify/speakingurl.js"></script>
        <script src="/js/slugify/slugify.min.js"></script>
        <script src="/js/angular-notify/dist/angular-notify.min.js"></script>

        <form role="form"
              action="@if(isset($dataTypeContent->id)){{ route('voyager.sliders.update', $dataTypeContent->id) }}@else{{ route('voyager.sliders.store') }}@endif"
              method="POST" enctype="multipart/form-data">
            <!-- PUT Method if we are editing -->
            @if(isset($dataTypeContent->id))
                {{ method_field("PUT") }}
            @endif

            {{ csrf_field() }}


            <div class="row">

                <div class="col-md-8">
                    <!-- ### TITLE ### -->
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title"> Название </h3>
                        </div>
                        <div class="panel-body">
                            <input type="text" id="title" class="form-control" name="title"
                                   placeholder="Назввание"
                                   value="@if(isset($dataTypeContent->title)){{ $dataTypeContent->title }}@endif">
                        </div>

                        <div class="panel-heading">
                            <h3 class="panel-title"> Ссылка </h3>
                        </div>

                        <div class="panel-body">
                            <input type="text" class="form-control" id="link" name="link" placeholder="link"
                                   value="@if(isset($dataTypeContent->link)){{ $dataTypeContent->link }}@endif">
                        </div>

                        <div class="panel-heading">
                            <h3 class="panel-title"> Изображение </h3>
                        </div>

                        <div class="panel-body">
                            @if(isset($dataTypeContent->id))
                                <div ng-init="gl.getImage('slider', '{{$dataTypeContent->id}}')">
                                    <div class="gallery-images">
                                        <div class="image" ng-show="gl.singleImage">
                                            <img ng-src="@{{ gl.singleImage.url }}" alt="">
                                            <div class="delete"
                                                 ng-click="gl.deleteBanner(gl.singleImage, '{{$dataTypeContent->id}}', 'slider')">
                                                x
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- ### IMAGES ### -->
                                <div class="add-more btn btn-primary"
                                     ngf-select="gl.uploadBanner($files , {{$dataTypeContent->id}},'slider')"
                                     ng-show="!gl.singleImage"
                                     accept="image/*">Добавить изображении
                                </div>
                            @else
                                <?='<i class="icon wb-plus-circle"></i>  Создайте сначало баннер'; ?>
                            @endif
                        </div>


                        <div class="panel-heading">
                            <h3 class="panel-title"> Скрыть/Показать </h3>
                        </div>
                        <div class="panel-body">
                            <input type="checkbox" name="active" @if(isset($dataTypeContent->active) && $dataTypeContent->active){{ 'checked="checked"' }}@endif class="toggleswitch">

                        </div>

                    </div>
                    <button type="submit" class="btn btn-primary pull-right">
                        @if(isset($dataTypeContent->id)){{ 'Обновить' }}@else<?= '<i class="icon wb-plus-circle"></i> Создать'; ?>@endif
                    </button>
                </div>
            </div>
        </form>

        <iframe id="form_target" name="form_target" style="display:none"></iframe>
        <form id="my_form" action="{{ route('voyager.upload') }}" target="form_target" method="post"
              enctype="multipart/form-data" style="width:0px;height:0;overflow:hidden">
            {{ csrf_field() }}
            <input name="image" id="upload_file" type="file" onchange="$('#my_form').submit();this.value='';">
            <input type="hidden" name="type_slug" id="type_slug" value="{{ $dataType->slug }}">
        </form>
    </div>
@stop

@section('javascript')
    <script>

        $('document').ready(function () {
            $('.toggleswitch').bootstrapToggle();
        })
    </script>
@stop
