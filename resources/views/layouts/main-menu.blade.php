<ul>
    @foreach(\App\Category::where('city', \App\City::getCookieCity())->where('active', 1)->orderBy('order', 'asc')->get() as $category)
        <li class="{{ Request::is('catalog/'. $category->slug) ? 'active' : '' }}">
            <a href="/catalog/{{ $category->slug }}">
                <div class="icon">
                    <img src="{{ Voyager::image($category->image) }}" class="svg" alt="">
                </div>
                <div class="name"> {{ $category->name }}</div>
            </a>
        </li>
    @endforeach
</ul>
