<!doctype html>
<html lang="ru" ng-app="rumiApp" ng-cloak>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title> RUMI - онлайн ресторан </title>
    <link rel="shortcut icon" type="image/x-icon" href="/images/favicon.png"/>
    <link rel="stylesheet" href="/js/angular-notify/dist/angular-notify.min.css">
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/main.css?v={{ \App\Helpers::gtv() }}">
    <link rel="stylesheet" href="/css/mobile.css?v={{ \App\Helpers::gtv() }}">
    <link rel="stylesheet" href="/css/slick.css?v={{ \App\Helpers::gtv() }}">
    <link rel="stylesheet" href="/css/slick-theme.css">
    <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU&amp;coordorder=longlat&amp;apikey=a2a8abd6-66b5-4670-8e3a-4df11e81e634"
            type="text/javascript"></script>
    <style type="text/css">
        .add-to-cart01 .btn-orange01{
            display: none !important;
        }
    </style>

    <!-- Facebook Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s)
            {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};
            if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
            n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t,s)}(window,document,'script',
            'https://connect.facebook.net/en_US/fbevents.js');
             fbq('init', '244413090024325'); 
            fbq('track', 'PageView');
        </script>
        <noscript>
            <img height="1" width="1" src="https://www.facebook.com/tr?id=244413090024325&ev=PageView&noscript=1"/>
        </noscript>
    <!-- End Facebook Pixel Code -->

</head>
<body>




<div class="main-wrapper" ng-controller="CartController as ct">
    <header id="header" class="header-wrapper" ng-controller="MainController as mn">

        <div class="container">
            <div class="wrapper visible-xs mobile-header">
                <div class="loggin-mobile" style="width: 0%; overflow: hidden" ng-controller="AuthController as au">
                    @if (Auth::guest())
                        <div class="auth-forms">
                            <div class="col-lg-7">
                                <div class="right-cols nav-modal-close">
                                    <div class="mobile-nav-open navbar-toggle">
                                        <span class="icon-bar left"></span>
                                        <span class="icon-bar right"></span>
                                    </div>
                                </div>
                                <form class="login-form" role="form" data-toggle="validator">
                                    <div class="form-group">
                                        <input type="email" required class="input-std" placeholder="Емейл"
                                               ng-model="au.email">
                                        <div class="help-block">@{{ au.emailerr }}</div>
                                    </div>

                                    <div class="form-group">
                                        <input type="password" required class="input-std" placeholder="Пароль"
                                               ng-model="au.pass">
                                        <div class="help">@{{ au.passerr }}</div>
                                    </div>

                                    <div class="buttons">
                                        <button type="submit" class="btn-blue login"
                                                ng-click="au.setForm()"> @lang('main.sign_in')
                                        </button>
                                        <a href="/registration"
                                           class="btn-clear register"> @lang('main.registration') </a>
                                    </div>
                                    <a href="/password/reset" class="reset-pass"> @lang('main.forgot_password') </a>
                                </form>
                            </div>
                            <div class="col-lg-5">
                                <div class="social-auth">
                                    <div class="title-form"> @lang('main.social_auth') </div>
                                    <div class="buttons">
                                        <a href="/auth/facebook" class="btn-std btn-fb">Facebook</a>
                                        <a href="/auth/vkontakte" class="btn-std btn-vk">Вконтакте</a>
                                        <a href="/auth/facebook" class="btn-std btn-google">Google +</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @else
                        <a href="#" class="logged-link">
                            <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 32 32">
                                        <path fill="#FFF" fill-rule="evenodd"
                                              d="M20.55 19.033c2.492-1.516 4.117-4.225 4.117-7.366C24.667 6.9 20.767 3 16 3c-4.767 0-8.667 3.9-8.667 8.667 0 3.141 1.625 5.85 4.117 7.366C7.117 20.658 3.867 24.45 3 29h2.167C6.25 24.017 10.692 20.333 16 20.333S25.75 24.017 26.833 29H29c-.867-4.658-4.117-8.45-8.45-9.967zM9.5 11.667c0-3.575 2.925-6.5 6.5-6.5s6.5 2.925 6.5 6.5-2.925 6.5-6.5 6.5-6.5-2.925-6.5-6.5z"/>
                                    </svg>
                        </a>
                        <div class="logged-user">
                            <ul>
                                <li><a href="/cabinet/profile"> @lang('main.my_profile') </a></li>
                                <li><a href="/cabinet/profile"> @lang('main.history_of_orders') </a></li>
                                <li><a href="/cabinet/settings"> @lang('main.settings_of_profile') </a></li>
                                <li><a href="/user/logout"> @lang('main.logout') </a></li>
                            </ul>
                        </div>
                    @endif
                </div>

                <div class="nav-mobile-wrapper" style="width: 0%; overflow: hidden;">
                    <div class="menu-controls">
                        <div class="col-xs-4">
                            <div class="right-cols nav-modal-close">
                                <div class="mobile-nav-open navbar-toggle">
                                    <span class="icon-bar left"></span>
                                    <span class="icon-bar right"></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-4">
                            <div class="logo">
                                <a href="/"><img src="/images/logo-web.png" alt=""></a>
                            </div>
                        </div>
                        <div class="col-xs-4">
                            <div class="cart-block">
                                <a href="/cart" class="cart-link">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 32 32">
                                        <path fill="#FFF" fill-rule="evenodd"
                                              d="M27.133 7.028a.89.89 0 0 0-.89-.805h-3.828C22.362 2.781 19.505 0 15.997 0 12.488 0 9.63 2.781 9.578 6.223H5.751a.886.886 0 0 0-.89.805L3 27.661C2.993 30.09 5.18 32 7.87 32h16.253C26.814 32 29 30.09 29 27.74c0-.026-1.867-20.712-1.867-20.712zM15.997 1.767c2.518 0 4.571 1.99 4.624 4.456h-9.249c.053-2.467 2.106-4.456 4.625-4.456zm8.126 28.466H7.87c-1.687 0-3.056-1.1-3.083-2.454L6.568 7.997h3.003v2.683c0 .49.4.883.897.883a.887.887 0 0 0 .897-.883V7.997h9.256v2.683c0 .49.399.883.897.883a.887.887 0 0 0 .897-.883V7.997h3.004l1.787 19.782c-.027 1.355-1.402 2.454-3.083 2.454z"/>
                                    </svg>
                                    <span class="count" ng-bind="ct.cartProductCount "></span>
                                </a>
                            </div>

                        </div>
                    </div>
                    <div class="phones-mobile">
                        @if(\App\City::getCookieCity() == 'almaty')
                            <a href="tel:+77018080111"> +7 701 808 01 11</a>
                        @else
                            <a href="tel:+77018029111"> +7 701 802 91 11</a>
                        @endif
                    </div>
                    <div class="btns-wrap">
                        @if (Auth::guest())
                            <div class="col-xs-6">
                                <a href="#" class="btn btn-blue" id="login-trigger">  @lang('main.sign_in') </a>
                            </div>
                            <div class="col-xs-6">
                                <a href="/registration" class="btn btn-clear"> @lang('main.registration')</a>
                            </div>
                        @else
                            <div class="col-xs-12">
                                <a href="/cabinet/profile" class="btn btn-blue" style="width: auto;"> Мой профиль </a>
                            </div>
                        @endif()

                    </div>

                    <div class="mobile-menu">
                        <nav class="navbar">
                            <div class="container-fluid">
                                <!-- Brand and toggle get grouped for better mobile display -->
                                <div class="navbar-header hide">
                                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                            data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                        <span class="sr-only">Toggle navigation</span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                    </button>
                                    <a class="navbar-brand" href="#">Brand</a>
                                </div>

                                <!-- Collect the nav links, forms, and other content for toggling -->
                                <div class="collapse navbar-collapse in" id="bs-example-navbar-collapse-1">
                                    <ul class="nav navbar-nav catalog-mobile-menu">
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                               aria-haspopup="true"
                                               aria-expanded="false"><span> @lang('main.menu') </span> <i
                                                        class="arrow"><img src="/images/icons/down-arrow.png"
                                                                           alt=""></i></a>
                                            <ul class="dropdown-menu">

                                                @foreach(\App\Category::where('city',\App\City::getCookieCity())->where('active', 1)->orderBy('order', 'desc')->get() as $category)
                                                    <li class="{{ Request::is('catalog/'. $category->slug) ? 'active' : '' }}">
                                                        <a href="/catalog/{{ $category->slug }}">
                                                            <div class="icon">
                                                                <img src="{{ Voyager::image($category->image) }}" class="svg" alt="">
                                                            </div>
                                                            <div class="name"> {{ $category->name }}</div>
                                                        </a>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </li>
                                        <li><a href="/delivery"><span> @lang('main.delivery') </span> </a></li>
                                        <li><a href="/contacts"><span> @lang('main.contacts') </span></a></li>
                                        <li class="city-select">
                                            <div class="caption"> @lang('main.city') </div>
                                            @include('layouts.select-city')
                                        </li>
                                        <li class="lang-select">
                                            <div class="caption"> @lang('main.site_lang') </div>
                                            @include('layouts.select-lang')
                                        </li>

                                    </ul>
                                </div>
                            </div>
                        </nav>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-4">
                        <div class="right-cols" id="nav-modal-trigger">
                            <div class="mobile-nav-open navbar-toggle">
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="logo">
                            <a href="/"><img src="/images/logo-web.png" alt=""></a>
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="cart-block">
                            <a href="/cart" class="cart-link">
                                <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 32 32">
                                    <path fill="#FFF" fill-rule="evenodd"
                                          d="M27.133 7.028a.89.89 0 0 0-.89-.805h-3.828C22.362 2.781 19.505 0 15.997 0 12.488 0 9.63 2.781 9.578 6.223H5.751a.886.886 0 0 0-.89.805L3 27.661C2.993 30.09 5.18 32 7.87 32h16.253C26.814 32 29 30.09 29 27.74c0-.026-1.867-20.712-1.867-20.712zM15.997 1.767c2.518 0 4.571 1.99 4.624 4.456h-9.249c.053-2.467 2.106-4.456 4.625-4.456zm8.126 28.466H7.87c-1.687 0-3.056-1.1-3.083-2.454L6.568 7.997h3.003v2.683c0 .49.4.883.897.883a.887.887 0 0 0 .897-.883V7.997h9.256v2.683c0 .49.399.883.897.883a.887.887 0 0 0 .897-.883V7.997h3.004l1.787 19.782c-.027 1.355-1.402 2.454-3.083 2.454z"/>
                                </svg>
                                <span class="count" ng-bind="ct.cartProductCount "></span>
                            </a>
                        </div>

                    </div>
                </div>
                <a href="http://uzynagash.kz" style="display:none">Узынагаш </a>
            </div>


            <div class="wrapper hidden-xs">
                <div class="row no-padding">
                    <div class="col-lg-6 col-md-4 col-sm-5">
                        <div class="logo">
                            <a href="/"><img src="/images/logo-web.png" alt=""></a>
                        </div>
                        <div class="menu">
                            @if(Route::currentRouteName() == 'catalogIndex')
                                <button class="active">
                                    <span class="icons">
                                        <i class="icon-bar"></i>
                                        <i class="icon-bar"></i>
                                        <i class="icon-bar"></i>
                                    </span>
                                    <span class="name"> @lang('main.menu') </span>
                                </button>
                            @else
                                <button ng-click="mn.mainmenu = !mn.mainmenu"
                                        ng-class="mn.mainmenu == true ? 'active' : ''" click-outside="mn.closeMenu()">
                                    <span class="icons">
                                        <i class="icon-bar"></i>
                                        <i class="icon-bar"></i>
                                        <i class="icon-bar"></i>
                                    </span>
                                    <span class="name"> @lang('main.menu') </span>
                                </button>
                            @endif
                        </div>
                        <div class="delivery hidden-sm">
                            <a href="/delivery"> @lang('main.delivery') </a>
                        </div>
                        <div class="delivery hidden-sm">
                            <a href="/contacts"> @lang('main.contacts') </a>
                        </div>
                    </div>
                    <div class="col-lg-2 col-md-2 hidden-sm">
                        <div class="phones">

                            @if(\App\City::getCookieCity() == 'almaty')
                                <a href="tel:+77018080111">+7 701 808 01 11</a>
                            @else
                                <a href="tel:+77018029111">+7 701 802 91 11</a>
                            @endif

                        </div>
                    </div>

                    <div class="col-lg-2  col-md-3 col-sm-4">
                        <div class="user-filter">
                            <div class="city">
                                @include('layouts.select-city')
                            </div>
                            <div class="lang">
                                @include('layouts.select-lang')
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2 col-md-3 col-sm-3">
                        <div class="cart-block">
                            <a href="#" class="cart-link" ng-click="mn.cartform = !mn.cartform"
                               ng-class="mn.cartform == true ? 'active' : ''" click-outside="mn.closeCartForm()"
                               outside-if-not="cartform">
                                <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 32 32">
                                    <path fill="#FFF" fill-rule="evenodd"
                                          d="M27.133 7.028a.89.89 0 0 0-.89-.805h-3.828C22.362 2.781 19.505 0 15.997 0 12.488 0 9.63 2.781 9.578 6.223H5.751a.886.886 0 0 0-.89.805L3 27.661C2.993 30.09 5.18 32 7.87 32h16.253C26.814 32 29 30.09 29 27.74c0-.026-1.867-20.712-1.867-20.712zM15.997 1.767c2.518 0 4.571 1.99 4.624 4.456h-9.249c.053-2.467 2.106-4.456 4.625-4.456zm8.126 28.466H7.87c-1.687 0-3.056-1.1-3.083-2.454L6.568 7.997h3.003v2.683c0 .49.4.883.897.883a.887.887 0 0 0 .897-.883V7.997h9.256v2.683c0 .49.399.883.897.883a.887.887 0 0 0 .897-.883V7.997h3.004l1.787 19.782c-.027 1.355-1.402 2.454-3.083 2.454z"/>
                                </svg>
                                <span class="count" ng-bind="ct.cartProductCount "></span>
                                <span class="summa"> @{{ ct.cartsumm }} ₸</span>
                            </a>
                            <div class="cart-form" ng-show="mn.cartform" id="cartform">
                                <div class="item" ng-repeat="product in ct.cart">
                                    <div class="col-lg-6">
                                        <div class="title"><span ng-bind="product.title"></span></div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="col-lg-6">
                                            <div class="product-count">
                                                <a href="#" ng-if="ct.cart[product.id].quantity != 1"
                                                   ng-click="ct.minus(product);$event.preventDefault()"><span
                                                            class="control minus"> - </span></a>
                                                <a href="#" ng-if="ct.cart[product.id].quantity == 1"
                                                   ng-click="$event.preventDefault()"><span
                                                            class="control minus"> - </span></a>
                                                <span class="qty ng-binding"> @{{ ct.cart[product.id].quantity || 0}} </span>
                                                <a href="#"
                                                   ng-click="ct.addToCart(product);$event.preventDefault()"><span
                                                            class="control plus"> + </span></a>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="price"
                                                 ng-bind="(product.price *  ct.cart[product.id].quantity) + ' ₸'"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="footer-form" ng-show="!ct.isEmpty(ct.cart)">

                                    <div class="col-lg-6"><a href="/checkout"
                                                             class="btn-orange"> @lang('main.сheckout') </a>
                                    </div>
                                </div>
                                <div ng-hide="!ct.isEmpty(ct.cart)" style="text-align: center; font-size: 18px">
                                    <i> Корзина пуста </i>
                                </div>
                            </div>

                        </div>
                        <div class="user-block" ng-controller="AuthController as au">
                            @if (Auth::guest())

                                <a href="#" class="link" ng-click="mn.authform = !mn.authform"
                                   ng-class="mn.authform == true ? 'active' : ''" click-outside="mn.closeAuthForm()"
                                   outside-if-not="authform">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 32 32">
                                        <path fill="#FFF" fill-rule="evenodd"
                                              d="M20.55 19.033c2.492-1.516 4.117-4.225 4.117-7.366C24.667 6.9 20.767 3 16 3c-4.767 0-8.667 3.9-8.667 8.667 0 3.141 1.625 5.85 4.117 7.366C7.117 20.658 3.867 24.45 3 29h2.167C6.25 24.017 10.692 20.333 16 20.333S25.75 24.017 26.833 29H29c-.867-4.658-4.117-8.45-8.45-9.967zM9.5 11.667c0-3.575 2.925-6.5 6.5-6.5s6.5 2.925 6.5 6.5-2.925 6.5-6.5 6.5-6.5-2.925-6.5-6.5z"/>
                                    </svg>
                                </a>

                                <div class="auth-forms" ng-show="mn.authform" id="authform">
                                    <div class="col-lg-7">
                                        <form ng-submit="au.setForm()" class="login-form" role="form"
                                              data-toggle="validator">
                                            <div class="title-form"> @lang('main.sign_in') </div>
                                            <div class="form-group">
                                                <input type="email" required class="input-std" ng-enter="au.setForm()"
                                                       placeholder="@lang('main.email')" ng-model="au.email">
                                                <div class="help-block">@{{ au.emailerr }}</div>
                                            </div>

                                            <div class="form-group">
                                                <input type="password" required class="input-std"
                                                       ng-enter="au.setForm()" placeholder="@lang('main.password')"
                                                       ng-model="au.pass">
                                                <div class="help">@{{ au.passerr }}</div>
                                            </div>

                                            <div class="buttons">
                                                <button type="submit"
                                                        class="btn-blue login"> @lang('main.sign_in') </button>
                                                <a href="/registration"
                                                   class="btn-clear register"> @lang('main.registration') </a>
                                            </div>
                                            <a href="/password/reset"
                                               class="reset-pass"> @lang('main.forgot_password') </a>
                                        </form>
                                    </div>
                                    <div class="col-lg-5">
                                        <div class="social-auth">
                                            <div class="title-form">  @lang('main.social_auth')</div>
                                            <div class="buttons">
                                                <a href="/auth/facebook" class="btn-std btn-fb">Facebook</a>
                                                <a href="/auth/vkontakte" class="btn-std btn-vk">Вконтакте</a>
                                                <a href="/auth/facebook" class="btn-std btn-google">Google +</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @else
                                <a href="#" class="logged-link" ng-click="mn.logged = !mn.logged"
                                   click-outside="mn.closeLoggedForm()" outside-if-not="logged">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 32 32">
                                        <path fill="#FFF" fill-rule="evenodd"
                                              d="M20.55 19.033c2.492-1.516 4.117-4.225 4.117-7.366C24.667 6.9 20.767 3 16 3c-4.767 0-8.667 3.9-8.667 8.667 0 3.141 1.625 5.85 4.117 7.366C7.117 20.658 3.867 24.45 3 29h2.167C6.25 24.017 10.692 20.333 16 20.333S25.75 24.017 26.833 29H29c-.867-4.658-4.117-8.45-8.45-9.967zM9.5 11.667c0-3.575 2.925-6.5 6.5-6.5s6.5 2.925 6.5 6.5-2.925 6.5-6.5 6.5-6.5-2.925-6.5-6.5z"/>
                                    </svg>
                                </a>
                                <div class="logged-user" ng-show="mn.logged" id="logged">
                                    <ul>
                                        <li><a href="/cabinet/profile"> @lang('main.my_profile') </a></li>
                                        <li><a href="/cabinet/profile"> @lang('main.history_of_orders') </a></li>
                                        <li><a href="/cabinet/settings"> @lang('main.settings_of_profile') </a></li>
                                        <li><a href="/user/logout"> @lang('main.logout') </a></li>
                                    </ul>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row no-padding">
                    <div class="col-lg-12">
                        @if(Route::currentRouteName() == 'catalogIndex')
                            <div class="catalog-menu catalog-rel">
                                @include('layouts.main-menu')
                            </div>
                        @else
                            <div class="catalog-menu catalog-abs" ng-show="mn.mainmenu">
                                @include('layouts.main-menu')
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </header>


    <div class="fixed-header header-wrapper hidden-xs hidden-md" set-class-when-at-top="fix-to-top">
        <div class="container">
            <div class="wrapper">
                <div class="row no-padding">
                    <div class="col-lg-4 col-sm-5">
                        <div class="logo">
                            <a href="/"><img src="/images/logo-web.png" alt=""></a>
                        </div>
                        <div class="menu">
                            <button ng-click="mn.mainmenu = !mn.mainmenu" ng-class="mn.mainmenu == true ? 'active' : ''"
                                    click-outside="mn.closeMenu()">
                                    <span class="icons">
                                        <i class="icon-bar"></i>
                                        <i class="icon-bar"></i>
                                        <i class="icon-bar"></i>
                                    </span>
                                <span class="name"> @lang('main.menu') </span>
                            </button>
                        </div>
                    </div>

                    <div class="col-lg-4 col-lg-offset-4 col-sm-7">
                        <div class="cart-block">
                            <a href="#" class="cart-link" ng-click="mn.fixedCartForm = !mn.fixedCartForm"
                               ng-class="mn.fixedCartForm == true ? 'active' : ''" click-outside="mn.closeFixedCart()"
                               outside-if-not="fixedCart">
                                <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 32 32">
                                    <path fill="#FFF" fill-rule="evenodd"
                                          d="M27.133 7.028a.89.89 0 0 0-.89-.805h-3.828C22.362 2.781 19.505 0 15.997 0 12.488 0 9.63 2.781 9.578 6.223H5.751a.886.886 0 0 0-.89.805L3 27.661C2.993 30.09 5.18 32 7.87 32h16.253C26.814 32 29 30.09 29 27.74c0-.026-1.867-20.712-1.867-20.712zM15.997 1.767c2.518 0 4.571 1.99 4.624 4.456h-9.249c.053-2.467 2.106-4.456 4.625-4.456zm8.126 28.466H7.87c-1.687 0-3.056-1.1-3.083-2.454L6.568 7.997h3.003v2.683c0 .49.4.883.897.883a.887.887 0 0 0 .897-.883V7.997h9.256v2.683c0 .49.399.883.897.883a.887.887 0 0 0 .897-.883V7.997h3.004l1.787 19.782c-.027 1.355-1.402 2.454-3.083 2.454z"/>
                                </svg>
                                <span class="count" ng-bind="ct.cartProductCount "></span>
                                <span class="summa"> @{{ ct.cartsumm }} ₸</span>
                            </a>
                            <div class="cart-form" ng-show="mn.fixedCartForm" id="fixedCart">
                                <div class="item" ng-repeat="product in ct.cart">
                                    <div class="col-lg-6">
                                        <div class="title"><span ng-bind="product.title"></span></div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="col-lg-6">
                                            <div class="product-count">
                                                <a href="#" ng-if="ct.cart[product.id].quantity != 1"
                                                   ng-click="ct.minus(product);$event.preventDefault()"><span
                                                            class="control minus"> - </span></a>
                                                <a href="#" ng-if="ct.cart[product.id].quantity == 1"
                                                   ng-click="$event.preventDefault()"><span
                                                            class="control minus"> - </span></a>
                                                <span class="qty ng-binding"> @{{ ct.cart[product.id].quantity || 0}} </span>
                                                <a href="#"
                                                   ng-click="ct.addToCart(product);$event.preventDefault()"><span
                                                            class="control plus"> + </span></a>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="price"
                                                 ng-bind="(product.price *  ct.cart[product.id].quantity) + ' ₸'"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="footer-form" ng-show="!ct.isEmpty(ct.cart)">

                                    <div class="col-lg-6"><a href="/checkout"
                                                             class="btn-orange"> @lang('main.сheckout') </a>
                                    </div>
                                </div>
                                <div ng-hide="!ct.isEmpty(ct.cart)" style="text-align: center; font-size: 18px">
                                    <i> Корзина пуста </i>
                                </div>
                            </div>
                        </div>

                        <div class="order-btn">
                            <div class="btn-wrap">
                                <a href="/checkout" class="btn-orange"> @lang('main.сheckout') </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="catalog-menu catalog-abs" ng-show="mn.mainmenu">
            @include('layouts.main-menu')
        </div>
    </div>

