<form action="/language" method="post">

    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <select name="locale" onchange="this.form.submit()">
        <option value="ru" @if(App::getLocale() == 'ru') selected @endif > Русский </option>
        <option value="kz" @if(App::getLocale() == 'kz') selected @endif > Қазақша </option>
        <option value="en" @if(App::getLocale() == 'en') selected @endif > English</option>
    </select>

</form>