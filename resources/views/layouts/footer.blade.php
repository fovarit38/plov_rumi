<div id="scroller"></div>
<footer>
    <div class="container">
        <div class="row no-padding">

            <div class="col-lg-4 col-lg-offset-4 visible-md visible-xs">
                <div class="right-block">

                    <div class="col-lg-5">
                        <div class="social">
                            <ul>
                                <li><a href="https://www.facebook.com/plovkz/" target="_blank" class="fb"></a></li>
                                <li>
                                    <a href="https://www.tripadvisor.ru/Restaurant_Review-g298251-d8369582-Reviews-RUMI-Almaty.html"
                                       target="_blank" class="tr"></a></li>
                                <li><a href="https://www.instagram.com/plovkz/" target="_blank" class="in"></a></li>
                            </ul>
                        </div>
                        <div class="row">
                            <div class="col-xs-6">
                                <div class="phones">
                                    <div class="title"> Алматы:</div>
                                    <ul>
                                        <li><span class="name"></span><a href="tel:+77018080111"><span>+7 (701) </span>
                                                808-01-11</a></li>

                                    </ul>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="phones">
                                    <div class="title"> Нур-Султан:</div>
                                    <ul>
                                        <li><a href="tel:+77018029111"><span>+7 (701) </span> 802-91-11</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="developed hidden-xs hidden-md">

                            <a href="https://razrabotka-saitov.kz" target="_blank" title="Создание сайтов - Fastsite">
                                Создание сайтов — <span> Fastsite </span> </a>

                            <!--                             <a style="display: none;" href="https://fastsite.kz" >Создание сайтов - Fastsite</a>
                             -->
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-4">
                <div class="logo-company">
                    <ul>
                        <li><img src="/images/logo-rumi.png" alt=""></li>
                        <li class="hide"><img src="/images/logo-madeni.png" alt=""></li>
                        <li><img src="/images/eac.png" alt=""></li>
                    </ul>
                </div>
                <div class="clearfix"></div>
                <div class="menu hidden-xs hidden-md">
                    <ul>
                        <li><a href="/delivery"> @lang('main.delivery_information')</a></li>
                        <li><a href="/about">  @lang('main.about_us')</a></li>
                        <li><a href="/contacts">  @lang('main.contacts')</a></li>
                    </ul>
                </div>
                <div class="clearfix"></div>
                <div class="copyright">
                    @lang('main.copyright')

                </div>
            </div>
            <div class="col-lg-8">
                <div class="right-block">
                    <div class="col-lg-4 hidden-xs hidden-md">
                        <div class="phones">
                            <div class="title"> Алматы:</div>
                            <ul>
                                <li><span class="name"></span><a href="tel:+77018080111"><span>+7 (701) </span>
                                        808-01-11</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-4 hidden-xs hidden-md">
                        <div class="phones">
                            <div class="title"> Нур-Султан:</div>

                            <ul>
                                <li><a href="tel:+77018029111"><span>+7 (701) </span> 802-91-11</a></li>

                            </ul>
                        </div>
                    </div>

                    <div class="col-lg-4">
                        <div class="social hidden-xs hidden-md">
                            <ul>
                                <li><a href="https://www.facebook.com/rumi.qazaqstan/" class="fb" target="_blank"></a>
                                </li>
                                <li>
                                    <a href="https://www.tripadvisor.ru/Restaurant_Review-g298251-d8369582-Reviews-RUMI-Almaty.html"
                                       class="tr" target="_blank"></a></li>
                                <li><a href="https://instagram.com/rumi.qazaqstan" class="in" target="_blank"></a></li>
                            </ul>
                        </div>
                        <div class="clearfix"></div>
                        <div class="developed ">

                            <a href="https://razrabotka-saitov.kz" target="_blank" title="Создание сайтов - Fastsite">
                                Создание сайтов — <span> Fastsite </span> </a>

                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</footer>

<div ng-class="{'show-block':!ct.isEmpty(ct.cart)}">
    <div class="mobile-cart visible-xs visible-md visible-lg hidden-lg" ng-show="!ct.isEmpty(ct.cart)">
        <div class="container">
            <div class="row">
                <div class="col-xs-6">
                    <div class="dish-count"> @{{ct.cartProductCount}} блюда на сумму:</div>
                    <div class="order-summ" ng-bind="ct.cartsumm +' ₸'"></div>
                </div>
                <div class="col-xs-6">
                    <div class="btn-wrap">
                        <a href="/checkout" class="btn btn-orange"> Оформить заказ </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="citymodal" tabindex="-1" role="dialog" aria-labelledby="citymodal">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <form action="/change/city" method="post" id="cityselect">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <h3 class="modal-title"> Выберите город: </h3>
                <div class="row">
                    <div class="col-md-6 col-xs-6 text-center">
                        <div class="input-wrap">
                            <input type="radio" name="city" id="apple_city" value="almaty" onclick="this.form.submit();ct.clearSession()">
                            <label for="apple_city" class="btn btn-primary">Алматы</label>
                        </div>
                    </div>
                    <div class="col-md-6 col-xs-6 text-center">
                        <div class="input-wrap">
                            <input type="radio" name="city" id="nur_city" value="astana" onclick="this.form.submit();  ct.clearSession()">
                            <label for="nur_city" class="btn btn-primary">Нур-Султан</label>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
</div>




<script src="/js/jquery-3.2.1.min.js"></script>

<script src="/js/bootstrap.min.js"></script>
<script src="/js/angular/angular.min.js"></script>
<script src="/js/app.module.js"></script>
<script src="/js/main.controller.js?v={{ \App\Helpers::gtv() }}"></script>
<script src="/js/auth.controller.js?v={{ \App\Helpers::gtv() }}"></script>
<script src="/js/catalog.controller.js?v={{ \App\Helpers::gtv() }}"></script>
<script src="/js/cart.controller.js?v={{ \App\Helpers::gtv() }}"></script>
<script src="/js/review.controller.js?v={{ \App\Helpers::gtv() }}"></script>
<script src="/js/slider.controller.js?v={{ \App\Helpers::gtv() }}"></script>
<script src="/js/cabinet.controller.js?v={{ \App\Helpers::gtv() }}"></script>
<script src="/node_modules/angular-local-storage/dist/angular-local-storage.min.js"></script>
<script src="/js/angular-notify/dist/angular-notify.min.js"></script>
<script src="/js/angular-click-outside/clickoutside.directive.js"></script>

<script src="/js/ng-file-upload/ng-file-upload.min.js"></script>
<script src="/js/ng-file-upload/ng-file-upload-shim.min.js"></script>
<script src="/js/ng-file-upload/ng-img-crop.js"></script>
<script src="/js/slick.min.js"></script>
<script src="/js/angular-ui-mask/dist/mask.min.js"></script>

<script src="/js/iiko.controller.js"></script>
<script>
    $(window).scroll(function () {
        if ($(this).scrollTop() > 200) {
            $('#scroller').fadeIn();
        } else {
            $('#scroller').fadeOut();
        }
    });
    $('#scroller').click(function () {
        $('body,html').animate({
            scrollTop: 0
        }, 400);
        return false;
    });


    $(document).ready(function () {
        $('[data-toggle="tooltip"]').tooltip();
    });

    $(document).ready(function () {


        // mobile menu
        $('#nav-modal-trigger').click(function () {
            $('.nav-mobile-wrapper').animate({width: "100%"}, 200);
            $('.nav-mobile-menu').delay(200).fadeIn(300);
        });

        $('#login-trigger').click(function () {
            $('.loggin-mobile').animate({width: "100%"}, 200);

        });

        $('.nav-modal-close').click(function () {
            $('.nav-mobile-wrapper').animate({width: "0%"}, 200);
            $('.loggin-mobile').animate({width: "0%"}, 200);

        });

        $('.parent-menu a').click(function () {
            $('.nav-mobile-wrapper').animate({width: "0%"}, 200);
        });

        // slider single product
        $(".slider-product").slick({

            autoplay: true,
            speed: 700,
            dots: true,
            arrows: false,
            draggable: false,
            customPaging: function (slider, i) {
                var thumb = $(slider.$slides[i]).data('thumb');
                return '<a><img src="' + thumb + '"></a>';
            },

            responsive: [
                {
                    breakpoint: 960,
                    customPaging: null,
                    settings: {
                        dots: false,
                        arrows: true,
                        infinite: false,
                        slidesToShow: 2,
                        slidesToScroll: 1

                    }
                },
                {
                    breakpoint: 768,
                    customPaging: null,
                    settings: {
                        dots: false,
                        arrows: true,
                        infinite: false,
                        slidesToShow: 1,
                        slidesToScroll: 1

                    }
                }

            ]
        });


        // main page slider
        $('.mainslider').slick({
            fade: true,
            autoplay: false,
            arrows: false,
            dots: true,
            customPaging: function (slider, i) {
                var thumb = $(slider.$slides[i]).data('thumb');
                return ' <a class="pager-item"></a>';
            }
        });

        // recent reviews slider
        $('.recent-reviews').slick({
            infinite: true,
            speed: 1000,
            slidesToShow: 3,
            slidesToScroll: 1,

            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 3
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        });

        $('.recommendslider').slick({
            infinite: true,
            slidesToShow: 3,
            slidesToScroll: 1,
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 3
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        });


        $('#orderForm').submit(function() {
            $(this).find("input[type='submit']").prop('disabled',true);
        });
    });


</script>

<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
   (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
   m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
   (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

   ym(55618978, "init", {
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true,
        webvisor:true
   });
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/55618978" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-171719565-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-171719565-1');
</script>


@if(!\Cookie::has('city'))
    <script>
        $(document).ready(function () {
            $('#citymodal').modal({
                keyboard: false,
                backdrop: 'static'
            })
        });

    </script>
@endif

{{--<script src="//code.jivosite.com/widget.js" data-jv-id="2F0DI8rQ6H" async></script>--}}


</body>
</html>