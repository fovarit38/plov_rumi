@include('layouts.header')

<div class="page">

    <div class="cabinet-page" ng-controller="CabinetController as cb">
        <div class="container">
            <div class="row no-padding">
                <div class="title-page"> @lang('cabinet.title')</div>
                <div class="clearfix"></div>
                <div class="col-lg-4">

                    <div class="user-block">
                        <div class="col-lg-2 col-xs-12">
                            <div class="image"><img src="/@{{ cb.user.avatar }}" alt=""></div>
                        </div>
                        <div class="col-lg-10 col-xs-12">
                            <div class="info">
                                <div class="name"> @{{ cb.user.name }} </div>
                                <div class="age"> @{{ cb.currentYear - cb.user.born }} года, @{{ genderUser.name }}</div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="more-info">
                            <div class="field">
                                <div class="col-lg-6 col-xs-6">
                                    <div class="name"> @lang('cabinet.date_of_registration') </div>
                                </div>
                                <div class="col-lg-6 col-xs-6">
                                    <div class="value"> {{ $user->date_register }}</div>
                                </div>
                            </div>

                            <div class="field">
                                <div class="col-lg-6 col-xs-6">
                                    <div class="name"> @lang('cabinet.posted_reviews') </div>
                                </div>
                                <div class="col-lg-6 col-xs-6">
                                    <div class="value"> {{ $user->reviews->count() }} @lang('cabinet.reviews')</div>
                                </div>
                            </div>
                            <div class="field">
                                <div class="col-lg-6 col-xs-6">
                                    <div class="name"> @lang('cabinet.my_orders') </div>
                                </div>
                                <div class="col-lg-6 col-xs-6">
                                    <div class="value"> {{ $user->orders->count() }} покупки</div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="col-lg-8">
                    <div class="right-block">
                        <div class="menu-cabinet">
                            <ul>
                                <li class="{{ Request::is('cabinet/profile') ? 'active' : '' }}"><a href="/cabinet/profile"> @lang('cabinet.history_of_orders') </a></li>
                                <li class="{{ Request::is('cabinet/contacts') ? 'active' : '' }}"><a href="/cabinet/contacts"> @lang('cabinet.address_and_phone') </a></li>
                                <li class="{{ Request::is('cabinet/settings') ? 'active' : '' }}"><a href="/cabinet/settings"> @lang('cabinet.settings_of_profile')  </a></li>
                            </ul>
                        </div>
                        <div class="content">
                            @yield('cabinet')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('layouts.footer')