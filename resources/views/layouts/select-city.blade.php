<form action="/change/city" method="post">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <select name="city" onchange="this.form.submit(); ct.clearSession()">
        <option value="almaty" @if( Cookie::get('city') == 'almaty' ) selected @endif> @lang('main.almaty') </option>
        <option value="astana" @if( Cookie::get('city') == 'astana' ) selected @endif> @lang('main.astana') </option>
    </select>
</form>