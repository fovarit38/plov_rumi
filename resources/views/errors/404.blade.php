@extends('errors::minimal')

@section('title', 'Страница не обнаружена')
@section('code', '404')
@section('message')
    Страница не обнаружена <br>
    <a href='https://plov.kz'>вернутся на сайт </a>
@endsection
