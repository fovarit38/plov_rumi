@extends('errors::minimal')

@section('title', __('Срок действия страницы истек'))
@section('code', '419')
@section('message')
    Срок действия страницы истек <br>
    <a href='https://plov.kz'>вернутся на сайт </a>
@endsection
