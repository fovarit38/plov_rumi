@extends('errors::minimal')

@section('title', __('Ошибка сервера'))
@section('code', '500')
@section('message')
    Ошибка сервера <br>
    <a href='https://plov.kz'>вернутся на сайт </a>
@endsection

