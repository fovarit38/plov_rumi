<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>@yield('title')</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <style>
        html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Nunito', sans-serif;
            font-weight: 100;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }
        .left{
            padding-right: 10px;
            border-right: 2px solid;
        }
        .right{
            margin-left: 15px;
        }
        .logo{
            width: 80px;
            border-radius: 100%;
            padding: 0 10px;
        }
        .code {
            font-size: 26px;
            padding: 0 15px 0 15px;
            text-align: center;
        }

        .message {
            font-size: 18px;
            text-align: center;
        }
    </style>
    <!-- Styles -->
</head>
<body>
<div class="flex-center position-ref full-height">
    <div class="left">
        <img class="logo" src="https://media-cdn.tripadvisor.com/media/photo-s/0d/7e/da/11/rumi-rumi-group.jpg" alt="Logo Icon" style="">
    </div>
    <div class="right">
        <div class="code">
            @yield('code')
        </div>
        <div class="message" style="padding: 10px;">
            @yield('message')
        </div>
    </div>
</div>
</body>
</html>
