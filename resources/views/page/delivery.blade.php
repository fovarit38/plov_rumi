@extends('layouts.page')

@section('content')
    <div class="delevery-page">
        <div class="container">
            <div class="row no-padding">
                <div class="col-lg-12">
                    <div class="title-page">
                        @lang('delivery.title')
                    </div>
                </div>
                <div class="clearfix"></div>

                @if(\App\City::getCookieCity() == 'almaty')

                    <div class="col-lg-6 col-md-6">
                        <div class="section">
                            <div class="section-title">
                                <div class="number">1</div>
                                <div class="title"> @lang('delivery.block_title_1') </div>
                            </div>
                            <div class="bold-title"> @lang('delivery.first_section.bold_title') </div>
                            <div class="section-text"> @lang('delivery.first_section.text')</div>

                            <div class="info info-summa">
                                <div>
                                    <div class="summa"> @lang('delivery.first_section.info_summa.summa') </div>
                                <div class="caption"> При заказе свыше 10 000 тенге, доставка бесплатная!</div>
                                </div>
                            </div>
                            <div class="info info-time">
                                <div>

                                    <div class="summa"> @lang('delivery.first_section.info_time.summa') </div>
                                <div class="caption"> @lang('delivery.first_section.info_time.caption') </div>
                                </div>
                            </div>

                        </div>
                        <div class="section">

                            <div class="bold-title"> @lang('delivery.second_section.bold_title') </div>
                            <div class="info info-summa">
                                <div>
                                    <div class="summa">  @lang('delivery.second_section.info_summa.summa') </div>
                                <div class="caption"> @lang('delivery.second_section.info_summa.caption') </div>
                                </div>
                            </div>
                            <div class="info info-time">
                                <div>
                                    <div class="summa"> @lang('delivery.second_section.info_time.summa') </div>
                                <div class="caption">  @lang('delivery.second_section.info_time.caption') </div>
                                </div>
                            </div>

                        </div>
                        <div class="section">

                            <div class="bold-title"> @lang('delivery.third_section.bold_title') </div>
                            <div class="info info-address">
                                <div class="summa">
                                    @lang('delivery.third_section.info_address.summa')
                                </div>
                                <div class="summa"></div>
<!--                                 <div class="caption"> @lang('delivery.third_section.info_address.caption') </div>
 -->                            </div>
                        </div>

                        <div class="section">
                            <div class="section-title">
                                <div class="number">2</div>
                                <div class="title">  @lang('delivery.block_title_2') </div>
                            </div>

                            <div class="info info-time">
                                <div>
                                    <div class="summa"> @lang('delivery.third_section.info_time.summa') </div>
                                    <div class="caption"> @lang('delivery.third_section.info_time.caption')</div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-6 col-md-6">
                        <div class="map hidden-xs hidden-md">
                            <img src="/images/almaty.png" alt="" class="img-responsive">
                        </div>

                        <div class="section hide">
                            <div class="section-title">
                                <div class="number"> 3</div>
                                <div class="title"> @lang('delivery.block_title_3') </div>
                            </div>

                            <div class="info info-summa">
                                <div>
                                     <div class="summa"> @lang('delivery.fourth_section.info_summa.summa') </div>
                                    <div class="caption"> @lang('delivery.fourth_section.info_summa.caption')  </div>
                                </div>

                            </div>
                        </div>


                        <div class="map visible-xs visible-md">

                            <img src="/images/almaty.png" alt="" class="img-responsive">

                        </div>
                    </div>

                @else

                    <div class="col-lg-6 col-md-6">
                        <div class="section">
                            <div class="section-title">
                                <div class="number">1</div>
                                <div class="title">Условия доставки</div>
                            </div>
                            <div class="bold-title"> В квадрате доставки:</div>
                            <div class="section-text">
                                В радиусе до Ул Керей Жанибек, до Мустафина, начало Сейфулина, до
                                Жумабаева.
                            </div>
                            <div class="info info-summa">
                               <div>
                                    <div class="summa">1000 тг</div>
                                    <div class="caption"> Стоимость доставки за пределами квадрата</div>
                               </div>
                            </div>
                            <div class="info info-time">
                               <div>
                                    <div class="summa">50-80 мин.</div>
                                    <div class="caption"> Время доставки зависит от растояния и загруженности дорог</div>
                               </div>
                            </div>
                        </div>


                        <div class="section">

                            <div class="bold-title"> За пределами квадрата:</div>
                            <div class="section-text">Коктал, Тлендиева, улы дала, конец Абылайхана, Жумабаева</div>
                            <div class="info info-summa">
                               <div>
                                    <div class="summa">1500 тг</div>
                                    <div class="caption"> Стоимость доставки за пределами квадрата</div>
                               </div>
                            </div>


                            <div class="section-text">Район Магнум</div>
                            <div class="info info-summa">
                                <div>
                                    <div class="summa">2000 тг</div>
                                <div class="caption"> Стоимость доставки за пределами квадрата</div>
                                </div>
                            </div>
                        </div>


                        <!--  <div class="section">

                             <div class="bold-title">Забрать самому:</div>
                             <div class="info info-address">
                                 <div class="summa">
                                     Адрес для самовывоза: г.Астана ул.Желтоксан, 2/2,  ТРЦ Mega silkway <br>
                                 </div>
                                 <div class="summa"></div>
                                 <div class="caption"> Самовызов с 10:00 до 23:45</div>
                             </div>
                         </div> -->

                        <div class="section">
                            <div class="section-title">
                                <div class="number">2</div>
                                <div class="title">Время доставки</div>
                            </div>

                            <div class="info info-time">
                                <div>
                                    <div class="summa"> с 10:00 до 22:00</div>
                                    <div class="caption"> Время доставки вашей еды</div>
                                </div>
                            </div>
                        </div>
                        <div class="section">
                            <div class="section-title">
                                <div class="number">3</div>
                                <div class="title">Миним.сумма заказа</div>
                            </div>

                            <div class="info info-summa">
                                <div>
                                    <div class="summa"> 3 000 ₸</div>
                                    <div class="caption"> Минимальная сумма заказа</div>
                                </div>
                            </div>
                        </div>
                        <div class="map visible-xs visible-md">

                            <img src="/images/astanamap.png" alt="" class="img-responsive">

                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6">
                        <div class="map hidden-xs hidden-md">

                            <img src="/images/astanamap.png" alt="" class="img-responsive">

                        </div>

                    </div>

                @endif


            </div>
        </div>
    </div>

@endsection


