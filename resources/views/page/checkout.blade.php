@extends('layouts.page')

@section('content')


    <style>
        .title-bk {
            font-family: ALSSchlangesans, sans-serif;
            font-size: 20px;
            font-weight: bold;
            line-height: 1.1;
            color: #ffffff;
        }

        .cart-page {
            width: 100%;
            margin-bottom: 25px;
            display: flex;
            flex-direction: column;
            margin-top: 20px;
        }
    </style>

    @if(\App\City::getCookieCity() == 'astana')
        <div id="file_zone" data-url="/app/voyager/{{Voyager::setting('zon_astana')}}"></div>
    @else
        <div id="file_zone" data-url="/app/voyager/{{Voyager::setting('zon_almaty')}}"></div>
    @endif
    <div class="checkout-page">
        <div class="container">

            <div class="row no-padding">

                <div class="col-lg-12">
                    <div class="title-page"> @lang('checkout.title')</div>
                </div>


                <form method="POST" action="{{ url('/new/order/') }}" name="orderForm" class=" " id="orderForm">
                    {{ csrf_field() }}
                    <div class="col-lg-7 col-md-7">


                        <div class="cart-page" style="width: 100%;">
                            <div class="title-bk">
                                ВАША КОРЗИНА
                            </div>
                            <div class="col-lg-12">
                                <div ng-hide="!ct.isEmpty(ct.cart)"><i> Корзина пуста </i></div>
                                <div class="product" ng-repeat="product in ct.cart" style="margin-right: 0;">
                                    <div class="image inline-wrapper">
                                        <img src="@{{ product.image }}" alt="">
                                    </div>

                                    <div class="info inline-wrapper">
                                        <div class="title"><span ng-bind="product.title"></span></div>
                                        <div class="text">1 порция, @{{ product.size }}</div>
                                    </div>
                                    <div class="product-count inline-wrapper">
                                        <a href="#" ng-if="ct.cart[product.id].quantity != 1"
                                           ng-click="ct.minus(product);$event.preventDefault()"><span
                                                    class="minus"> - </span></a>
                                        <a href="#" ng-if="ct.cart[product.id].quantity == 1"
                                           ng-click="$event.preventDefault()"><span class="minus"> - </span></a>
                                        <span class="count" ng-bind="ct.cart[product.id].quantity || 0"></span>
                                        <a href="#" ng-click="ct.addToCart(product);$event.preventDefault()"><span
                                                    class="plus"> + </span></a>
                                    </div>

                                    <div class="price inline-wrapper"
                                         ng-bind="ct.cart[product.id].quantity * ct.cart[product.id].price + ' ₸'"></div>
                                    <div class="delete inline-wrapper">
                                        <a href="#" ng-click="ct.delete(ct.cart[product.id].id)">
                                            <img src="/images/icons/icon-del.svg" alt="" class="lg">
                                            <img src="/images/icons/del-xs.png" alt="" class="xs hide">
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="section section-first">
                            <div class="title"><span>1</span> @lang('checkout.your_data'):</div>

                            <div ng-class="{'has-content': ct.name.length}" class="input input-effect">
                                <input type="text"
                                       name="name"
                                       ng-init="ct.name = '{{ Auth::check() == true ? Auth::user()->name : '' }}'"
                                       ng-model="ct.name" required
                                       class="form-control input-lg name">
                                <label for=""> @lang('cabinet.name')</label>
                            </div>

                            <div ng-class="{'has-content': ct.userphone.length}" class="input input-effect">
                                <input type="text"
                                       ui-mask="+7 (999) 999-99-99"
                                       minlength="4" name="phone"
                                       ng-init="ct.phone = '{{ Auth::check() == true ? Auth::user()->phone : '' }}'"
                                       ng-model="ct.phone" id="phone" required
                                       class="form-control input-lg phone">
                                <label for=""></label>
                            </div>

                            <div ng-class="{'has-content': ct.email.length}" class="input input-effect">
                                <input type="text" name="email"
                                       ng-init="ct.email = '{{ Auth::check() == true && Auth::user()->email ? Auth::user()->email : ''}}'"
                                       ng-model="ct.email" required
                                       class="form-control input-lg email">
                                <label for="">Email</label>
                            </div>
                        </div>

                        <div class="section section-second">
                            <div class="title"><span>2</span> @lang('checkout.address_and_delivery'):</div>

                            <div class="delivery-type">

                                <input type="radio"
                                       id="d_type1"
                                       ng-model="ct.delivery"
                                       name="delivery"
                                       ng-value="1"
                                       ng-checked="1">
                                <label for="d_type1"> @lang('checkout.courier_delivery')</label>

                                <input type="radio"
                                       id="d_type2"
                                       ng-model="ct.delivery"
                                       name="delivery"
                                       ng-value="0"
                                       ng-click="ct.setSquare(0);ct.square= null">
                                <label for="d_type2"> @lang('checkout.pickup')</label>

                            </div>

                            <div ng-show="ct.delivery">

                                <div ng-class="{'has-content': ct.city.length}" class="input city">
                                    <input type="text" id="city" name="city" disabled
                                           ng-init="ct.city = '{{ \App\City::getCookieCity() == 'almaty' ? 'Алматы': 'Астана' }}'"
                                           ng-model="ct.city"
                                           class="form-control input-lg ">
                                    <label for="">Ваш город</label>
                                </div>

                                <div ng-class="{'has-content': ct.street.length}" class="input street input-effect"
                                     style="position: relative;">

                                    <input style="" type="search" id="suggest" name="street"
                                           ng-init="ct.street = '{{ Auth::check() == true && Auth::user()->street ? Auth::user()->street : '' }}'"
                                           ng-model="ct.street" autocomplete="off"
                                           class="form-control input-lg"
                                           ng-required="ct.delivery">

                                    <input type="text" id="price_out" required ng-required="ct.priceis" name="delivery_amount"
                                           value="" style="opacity: 0;visibility: hidden;"
                                           class="price_inputpps">
                                    <label for=""> Адрес </label>
                                </div>

                                {{--                                <div ng-class="{'has-content': ct.home.length}" class="input home">--}}
                                {{--                                    <input type="text" name="home"--}}
                                {{--                                           ng-init="ct.home = '{{ Auth::check() == true && Auth::user()->home ? Auth::user()->home : '' }}'"--}}
                                {{--                                           ng-model="ct.home"--}}
                                {{--                                           class="form-control input-lg"--}}
                                {{--                                           ng-required="ct.delivery">--}}
                                {{--                                    <label for="">@lang('cabinet.home') </label>--}}
                                {{--                                </div>--}}
                                <div ng-class="{'has-content': ct.apartment.length}" class="input apartment">
                                    <input type="text" name="apartment"
                                           ng-init="ct.apartment = '{{ Auth::check() == true && Auth::user()->apartment ? Auth::user()->apartment : '' }}'"
                                           ng-model="ct.apartment"
                                           class="form-control input-lg ">
                                    <label for=""> @lang('cabinet.apartment') </label>
                                </div>
                                <div ng-class="{'has-content': ct.entrance.length}" class="input entrance">
                                    <input type="text" name="entrance"
                                           ng-init="ct.entrance = '{{ Auth::check() == true && Auth::user()->entrance  ? Auth::user()->entrance : '' }}'"
                                           ng-model="ct.entrance"
                                           class="form-control input-lg">
                                    <label for="">@lang('cabinet.entrance')</label>
                                </div>
                                <div ng-class="{'has-content': ct.floor.length}" class="input floor">
                                    <input type="text" name="floor"
                                           ng-init="ct.floor = '{{ Auth::check() == true && Auth::user()->floor  ? Auth::user()->floor : '' }}'"
                                           ng-model="ct.floor"
                                           class="form-control input-lg">
                                    <label for="">@lang('cabinet.floor')</label>
                                </div>
                                <div ng-class="{'has-content': ct.doorphone.length}" class="input doorphone">
                                    <input type="text" name="doorphone"
                                           ng-init="ct.doorphone = '{{ Auth::check() == true && Auth::user()->doorphone  ? Auth::user()->doorphone : '' }}'"
                                           ng-model="ct.doorphone"
                                           class="form-control input-lg">
                                    <label for="">@lang('cabinet.doorphone_code')</label>
                                </div>

                                <div class="input time">
                                    <input type="time" name="time"
                                           ng-model="ct.time" class="form-control input-lg">
                                    <label for="">Время доставки:</label>
                                </div>

                                <div class="adresVised" style="margin-top: 30px;">
                                    <div class="title-bk">
                                        ВАШ АДРЕС НА КАРТЕ
                                    </div>
                                    <div id="map" style=" width: 40rem; height: 40rem; "></div>
                                </div>
                            {{--                                <p id="notice">Адрес не найден</p>--}}
                            {{--                                <div id="footer">--}}
                            {{--                                    <div id="messageHeader"></div>--}}
                            {{--                                    <div id="message"></div>--}}
                            {{--                                </div>--}}
                            <!-- <textarea name="comments" style="color: white;min-height: 75px; min-width:250px; background-color: rgba(113,113,113,0);" placeholder="@lang('cabinet.comments')" form="orderForm01"></textarea> -->
                            </div>


                        </div>

                        @if(\App\City::getCookieCity() == 'almaty')
                            <div class="section section-four" ng-show="!ct.delivery">
                                @foreach(\App\Shop::where("address","almaty")->get() as $ci)
                                    <label class="radio_chs">
                                        {{$ci->name}}
                                        <input type="radio"
                                               ng-model="ct.pickup"
                                               name="pickup"
                                               ng-value="{{$ci->id}}">
                                        <span
                                                class="checkmark"></span>
                                    </label>
                                @endforeach
                            </div>
                        @endif

                        @if(\App\City::getCookieCity() == 'astana')
                            <div class="section section-four" ng-show="!ct.delivery">
                                @foreach(\App\Shop::where("address","astana")->get() as $ci)
                                    <label class="radio_chs">
                                        {{$ci->name}}
                                        <input type="radio"
                                               ng-model="ct.pickup"
                                               name="pickup"
                                               ng-value="{{$ci->id}}"
                                               ng-checked="{{$ci->id}}">
                                        <span
                                                class="checkmark"></span>
                                    </label>
                                @endforeach
                            </div>
                        @endif

                    </div>
                    <div class="col-lg-4 col-lg-offset-1 col-md-5">
                        <div class="right-block">
                            <div class="title-wrap">
                                <div class="title"> @lang('checkout.total_amount'):</div>
                            </div>
                            <div class="section promocode">
                                <div class="input">
                                    <input type="text" name="promocode" ng-model="ct.code"
                                           placeholder="введите промокод" autocomplete="off">
                                    <button type="button" ng-click="ct.applyPromocode()"> применить</button>
                                </div>
                                <div class="info">
                                    <div ng-if="ct.promocode" class="text-sm">
                                        <span><img src="/images/done.png" alt=""></span>
                                        <span style="color: #28a745">Промокод активирован: @{{  ct.promocode.title }}</span>
                                    </div>
                                    <div ng-if="ct.errorData" class="text-sm">
                                        <span><img src="/images/error.png" alt=""></span>
                                        <span style="color: #dc3545" ng-bind="ct.errorData"> </span>
                                    </div>

                                    <div class="text-sm">
                                        * При введении промо кода, Вы можете использовать скидку/акцию/подарок на
                                        оформление заказа
                                    </div>
                                </div>
                            </div>

                            <div class="summa">
                                <div class="field">
                                    <div class="col-lg-6 col-xs-6 col-md-6">
                                        <div class="name"> @lang('checkout.order_price'):</div>
                                    </div>
                                    <div class="col-lg-6 col-xs-6 col-md-6">
                                        <div class="value"> @{{ ct.cartsumm }} ₸</div>
                                    </div>
                                </div>
                                <div class="field">
                                    <div class="col-lg-6 col-xs-6 col-md-6">
                                        <div class="name"> @lang('checkout.shipping_amount'):</div>
                                    </div>
                                    <div class="col-lg-6 col-xs-6 col-md-6">
                                        <div class="value"> @{{ ct.deliverysumm }} ₸</div>
                                    </div>
                                </div>
                                <div class="field" ng-if="ct.promocode">
                                    <div ng-if="ct.promocode.type == 'discount'">
                                        <div class="col-lg-6 col-xs-6 col-md-6">
                                            <div class="name"> Скидка</div>
                                        </div>
                                        <div class="col-lg-6 col-xs-6 col-md-6">
                                            <div class="value"> - @{{ ct.promocode.discount }}%</div>
                                        </div>
                                    </div>
                                    <div ng-if="ct.promocode.type == 'sum'">
                                        <div class="col-lg-6 col-xs-6 col-md-6">
                                            <div class="name"> Скидка</div>
                                        </div>
                                        <div class="col-lg-6 col-xs-6 col-md-6">
                                            <div class="value"> - @{{ ct.promocode.sum}} ₸</div>
                                        </div>
                                    </div>
                                </div>

                                <div class="field total">
                                    <div class="col-lg-6 col-md-6 col-xs-6">
                                        <div class="name"> К оплате:</div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-xs-6">
                                        <div class="value"> @{{ ct.totalSumm }} ₸</div>

                                        <input type="hidden" name="amount" value="@{{ ct.cartsumm }}">
                                        <input type="hidden" name="total_amount" value="@{{ ct.totalSumm }}">
                                        <input type="hidden" name="products" value="@{{ ct.cart }}">
                                    </div>


                                </div>
                            </div>
                            <div class="payment">
                                <div class="section-title"> @lang('checkout.payment'):</div>
                                <div class="payment-type" ng-init="ct.payment = 'in-cash'">
                                    <input type="radio" id="payment1" name="payment" value="in-cash"
                                           ng-model="ct.payment">
                                    <label for="payment1"> @lang('checkout.in_cash') </label>

                                    <input type="radio" id="payment2" name="payment" value="online"
                                           ng-model="ct.payment">
                                    <label for="payment2">Онлайн</label>
                                </div>
                            </div>
                            <div class="section" ng-if="ct.payment == 'in-cash'">
                                <div ng-class="{'has-content': ct.zdacha.length}" class="input zdacha">
                                    @lang('checkout.short_change')
                                    <textarea ng-required="payment == 'in-cash'"
                                              ng-init="ct.zdacha = '{{ Auth::check() == true && Auth::user()->zdacha  ? Auth::user()->zdacha : '' }}'"
                                              ng-model="ct.zdacha"
                                              name="zdacha"
                                              class="form-control input-lg"
                                              placeholder="@lang('cabinet.comments')"></textarea>
                                </div>
                            </div>


                            <div class="btn-wrap">

                                <input type="submit" class="btn btn-orange" id="send" value="Сделать заказ"
                                       ng-disabled="(ct.cartsumm > 3000 && (ct.delivery==1?ct.priceis>0:(ct.pickup==0?false:true))) ? false : true">

                            </div>
                            <br>

                            <div class="alert alert-warning" ng-show="ct.cartsumm > 3000 ? false: true">
                                Минимальная сумма заказа <strong> 3000 тг </strong>
                            </div>

                            <div class="alert alert-warning" ng-show="(ct.delivery==1?ct.priceis>0:true) ? false: true">
                                Адрес находится вне зоны доставки
                            </div>


                            <div class="alert alert-warning" ng-show="ct.delivery==0?ct.pickup==0:false">
                                Выберите адрес самовывоза
                            </div>


                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <script>

        window.parInt = function (number) {
            return parseInt(String(number).replace(/[^-0-9]/gim, '').replace(/\s/g, ''));
        }

        ymaps.ready(search);

        window.hide_error = function (code) {
            ct.priceis = code;
            $("body").click();
            if (code == 1) {
                $(".adresVised").addClass("active")
            } else {
                $(".adresVised").removeClass("active")
            }
        }

        function search() {
            // Подключаем поисковые подсказки к полю ввода.
            var suggestView = new ymaps.SuggestView('suggest', {
                    provider: {
                        suggest: (function (request, options) {
                            return ymaps.suggest("Казахстан " + document.getElementById('city').value + ", " + request);
                        })
                    }
                }),
                map,
                placemark;

            // При клике по кнопке запускаем верификацию введёных данных.
            // $('#suggest').on('focusout', function (e) {
            //     geocode();
            // });

            setInterval(function () {
                geocode();
            }, 1000);

            function geocode() {
                // Забираем запрос из поля ввода.
                var request = "Казахстан " + document.getElementById('city').value + ", " + $('#suggest').val();
                // Геокодируем введённые данные.

                ymaps.geocode(request).then(function (res) {
                    var obj = res.geoObjects.get(0),
                        error, hint;

                    if (obj) {
                        // Об оценке точности ответа геокодера можно прочитать тут: https://tech.yandex.ru/maps/doc/geocoder/desc/reference/precision-docpage/
                        switch (obj.properties.get('metaDataProperty.GeocoderMetaData.precision')) {
                            case 'exact':
                                break;
                            case 'number':
                            case 'near':
                            case 'range':
                                error = 'Неточный адрес, требуется уточнение';
                                window.hide_error(0);
                                hint = 'Уточните номер дома';
                                break;
                            case 'street':
                                error = 'Неполный адрес, требуется уточнение';
                                window.hide_error(0);
                                hint = 'Уточните номер дома';
                                break;
                            case 'other':
                            default:
                                error = 'Неточный адрес, требуется уточнение';
                                window.hide_error(0);
                                hint = 'Уточните адрес';
                        }
                    } else {
                        error = 'Адрес не найден';
                        hint = 'Уточните адрес';
                        window.hide_error(0);
                    }

                    // Если геокодер возвращает пустой массив или неточный результат, то показываем ошибку.
                    if (error) {
                        showError(error);
                        showMessage(hint);
                    } else {
                        showResult(obj);
                    }
                }, function (e) {
                    console.log(e)
                })

            }

            function showResult(obj) {
                // Удаляем сообщение об ошибке, если найденный адрес совпадает с поисковым запросом.
                $('#suggest').removeClass('input_error');
                $('#notice').css('display', 'none');

                var mapContainer = $('#map'),
                    bounds = obj.properties.get('boundedBy'),
                    // Рассчитываем видимую область для текущего положения пользователя.
                    mapState = ymaps.util.bounds.getCenterAndZoom(
                        bounds,
                        [mapContainer.width(), mapContainer.height()]
                    ),
                    // Сохраняем полный адрес для сообщения под картой.
                    address = [obj.getCountry(), obj.getAddressLine()].join(', '),
                    // Сохраняем укороченный адрес для подписи метки.
                    shortAddress = [obj.getThoroughfare(), obj.getPremiseNumber(), obj.getPremise()].join(' ');
                // Убираем контролы с карты.
                mapState.controls = [];
                mapState.zoom = 14
                // Создаём карту.
                createMap(mapState, shortAddress);
                // Выводим сообщение под картой.
                showMessage(address);
            }

            function showError(message) {
                $('#notice').text(message);
                $('#suggest').addClass('input_error');
                $('#notice').css('display', 'block');
                // Удаляем карту.
                if (map) {
                    map.destroy();
                    map = null;
                }
            }


            function highlightResult(obj) {
                // Сохраняем координаты переданного объекта.
                var coords = obj.geometry.getCoordinates(),
                    // Находим полигон, в который входят переданные координаты.
                    polygon = window.deliveryZones.searchContaining(coords).get(window.deliveryZones.searchContaining(coords).getLength() - 1);
                if (polygon) {
                    // Уменьшаем прозрачность всех полигонов, кроме того, в который входят переданные координаты.
                    window.deliveryZones.setOptions('fillOpacity', 0.4);
                    polygon.options.set('fillOpacity', 0.8);
                    // Перемещаем метку с подписью в переданные координаты и перекрашиваем её в цвет полигона.
                    window.deliveryPoint.geometry.setCoordinates(coords);
                    window.deliveryPoint.options.set('iconColor', polygon.properties.get('fill'));
                    // Задаем подпись для метки.
                    if (typeof (obj.getThoroughfare) === 'function') {

                        setData(obj);
                    } else {

                        // Если вы не хотите, чтобы при каждом перемещении метки отправлялся запрос к геокодеру,
                        // закомментируйте код ниже.
                        ymaps.geocode(coords, {results: 1}).then(function (res) {
                            var obj = res.geoObjects.get(0);

                            setData(obj);
                        });
                    }
                } else {
                    // Если переданные координаты не попадают в полигон, то задаём стандартную прозрачность полигонов.
                    window.deliveryZones.setOptions('fillOpacity', 0.4);
                    // Перемещаем метку по переданным координатам.
                    window.deliveryPoint.geometry.setCoordinates(coords);
                    // Задаём контент балуна и метки.
                    window.deliveryPoint.properties.set({
                        iconCaption: 'Доставка недоступна',
                        balloonContent: 'Cвяжитесь с оператором',
                        balloonContentHeader: ''
                    });
                    $("#price_out").val(0);
                    window.setsumm(0)
                    window.hide_error(1);
                    // Перекрашиваем метку в чёрный цвет.
                    window.deliveryPoint.options.set('iconColor', 'black');
                }

                function setData(obj) {

                    var address = [obj.getThoroughfare(), obj.getPremiseNumber(), obj.getPremise()].join(' ');
                    if (address.trim() === '') {
                        address = obj.getAddressLine();
                    }

                    var price = polygon.properties.get('description');

                    var priceSplot = price.split(" ");

                    priceSplot[0] = window.parInt(priceSplot[0])
                    price = priceSplot[0];
                    // price = price.match(/<price>(.+)<\/price>/)[1];
                    $("#price_out").val(price);
                    window.setsumm(parseInt(price))
                    window.hide_error(1);
                    window.deliveryPoint.properties.set({
                        iconCaption: address,
                        balloonContent: address,
                        balloonContentHeader: price
                    });
                }
            }

            function onZonesLoad(json) {

                window.deliveryZones = ymaps.geoQuery(json).addToMap(map);
                // Задаём цвет и контент балунов полигонов.
                deliveryZones.each(function (obj) {
                    obj.options.set({
                        fillColor: obj.properties.get('fill'),
                        fillOpacity: obj.properties.get('fill-opacity'),
                        strokeColor: obj.properties.get('stroke'),
                        strokeWidth: obj.properties.get('stroke-width'),
                        strokeOpacity: obj.properties.get('stroke-opacity')
                    });
                    obj.properties.set('balloonContent', obj.properties.get('description'));
                });
                highlightResult(window.deliveryPoint);

            }


            function createMap(state, caption) {
                // Если карта еще не была создана, то создадим ее и добавим метку с адресом.

                // polygon = deliveryZones.searchContaining(coords).get(0);
                if (!map) {

                    map = new ymaps.Map('map', state);
                    window.deliveryPoint = new ymaps.Placemark(
                        map.getCenter(), {
                            iconCaption: caption,
                            balloonContent: caption
                        }, {
                            preset: 'islands#redDotIconWithCaption'
                        });
                    map.geoObjects.add(window.deliveryPoint);
                    // Если карта есть, то выставляем новый центр карты и меняем данные и позицию метки в соответствии с найденным адресом.

                    $.ajax({
                        url: $("#file_zone").data("url"),
                        dataType: 'json',
                        success: onZonesLoad
                    });

                } else {
                    map.setCenter(state.center, state.zoom);
                    window.deliveryPoint.geometry.setCoordinates(state.center);
                    window.deliveryPoint.properties.set({iconCaption: caption, balloonContent: caption});
                    highlightResult(window.deliveryPoint);
                }
            }

            function showMessage(message) {
                $('#messageHeader').text('Данные получены:');
                $('#message').text(message);
            }


        }


    </script>
    <style>
        ymaps {
            color: #000;
        }

        #map {
            margin-top: 2rem;
        }

        .price_inputpps {
            position: absolute;
            left: 0;
            top: 0;
            width: 100%;
            height: 100%;
            opacity: 0;
            height: 1px;
            top: 97%;
            padding: 0;
        }


        .radio_chs {
            display: block;
            position: relative;
            padding-left: 35px;
            margin-bottom: 24px;
            cursor: pointer;
            font-size: 22px;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        /* Hide the browser's default checkbox */
        .radio_chs input {
            position: absolute;
            opacity: 0;
            cursor: pointer;
            height: 0;
            width: 0;
        }

        /* Create a custom checkbox */
        .checkmark {
            position: absolute;
            top: 4px;
            left: 0;
            height: 25px;
            width: 25px;
            background-color: #eee;
        }

        /* On mouse-over, add a grey background color */
        .radio_chs:hover input ~ .checkmark {
            background-color: #ccc;
        }

        /* When the checkbox is checked, add a blue background */
        .radio_chs input:checked ~ .checkmark {
            background-color: #ff9500;
        }

        /* Create the checkmark/indicator (hidden when not checked) */
        .checkmark:after {
            content: "";
            position: absolute;
            display: none;
        }

        /* Show the checkmark when checked */
        .radio_chs input:checked ~ .checkmark:after {
            display: block;
        }

        /* Style the checkmark/indicator */
        .radio_chs .checkmark:after {
            left: 9px;
            top: 5px;
            width: 7px;
            height: 12px;
            border: solid white;
            border-width: 0 3px 3px 0;
            -webkit-transform: rotate(45deg);
            -ms-transform: rotate(45deg);
            transform: rotate(45deg);
        }

        .adresVised {
            position: relative;
            display: none;
        }

        .adresVised.active {
            display: block;
        }

        .adresVised:after {
            content: '';
            position: absolute;
            left: 0;
            top: 0;
            width: 100%;
            height: 100%;
        }
    </style>
@endsection