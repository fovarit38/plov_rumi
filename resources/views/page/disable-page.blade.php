<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title> RUMI - сеть ресторанов </title>
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/main.css?v={{ \App\Helpers::gtv() }}">
    <style>
        .logo {
            margin-top: 20px;
            margin-bottom: 20px;
            text-align: center;
        }
        h3{
            line-height: 1.4;
            font-size: 21px;
        }


        @media screen and (max-width: 1024px) {
            .logo {
                text-align: center;
            }

            .content {
                margin-top: 15px;
                text-align: center;
            }

            h1 {
                font-size: 28px;
            }

            h3 {
                margin-bottom: 20px;
                font-size: 16px;
            }

        }
    </style>
</head>
<body>

<div class="container">
    <div class="row">
        <div class="">
            <div class="text-left" style="margin-top: 50px">
                <div class="col-md-3 col-xs-12">
                    <div class="logo">
                        <img src="/images/logo-web.png" alt="" width="110px">
                    </div>
                </div>
                <div class="col-md-9 col-xs-12">
                    <div class="content">
                        <h1>{{ $page->title }}</h1>
                        <h3>{!! $page->body !!}</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>