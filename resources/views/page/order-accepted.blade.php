@extends('layouts.page')


@section('content')

    <div class="accepted-order" ng-init="ct.clearSession()">
        <div class="container">
            <div class="row no-padding">
                <h1> Ваш заказ принят </h1>

                <h4 style="margin-top: 25px;margin-bottom: 25px;">
                    Спасибо, Ваш заказ принят! В ближайшее время с Вами свяжется наш оператор.
                </h4>
                <div class="col-lg-3">
                    <table class="table">
                        <thead>
                        <tr>
                            <th></th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td> Номер заказа:</td>
                            <td> {{ $order->id }}</td>
                        </tr>
                        <tr>
                            <td> Сумма заказа:</td>
                            <td> {{ $order->total_amount }}</td>
                        </tr>

                            @if($order->promocode)

                                <tr>
                                    <td>Промокод: </td>
                                    <td> {{$order->promocode->title}}</td>
                                </tr>
                            @endif

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection