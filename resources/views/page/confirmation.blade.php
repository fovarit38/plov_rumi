@extends('layouts.page')


@section('content')

    <div class="accepted-order">
        <div class="container">
            <div class="row no-padding">
                <div class="page-title"> Подтверждение оплаты</div>

                <form name="SendOrder" method="post" action="https://testpay.kkb.kz/jsp/process/logon.jsp">
                    <input type="hidden" name="Signed_Order_B64" value="{{ $content }}">
                    E-mail: <input type="hidden" name="email" size=50 maxlength=50 value="tech@intelit.kz">
                    <input type="hidden" name="Language" value="ru"> <!-- язык формы оплаты rus/eng -->
                    <input type="hidden" name="BackLink" value="<?= config('app.url')?>/confirmation">
                    <input type="hidden" name="PostLink" value="<?= config('app.url')?>/kkb/postlink">
                    Со счетом согласен (-а)<br>
                    <input type="submit" name="GotoPay" value="Да, перейти к оплате" class="btn btn-default">
                </form>


            </div>
        </div>
    </div>

@endsection
