@extends('layouts.page')

@section('content')

    <div class="page static-page">
        <div class="container">
            <div class="row no-padding">
                <div class="col-lg-12">
                    <div class="title-page"> {{ $page->title }}</div>

                    <div class="content">
                        {!! $page->body !!}
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection