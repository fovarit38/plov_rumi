@extends('layouts.page')

@section('content')

    <div class="home-page">
        <div class="slider-block container" ng-controller="SliderController as sl">
            {{--            <ul class="items-slider mainslider">--}}
            {{--                @foreach($products as $product)--}}
            {{--                    <li class="item">--}}
            {{--                        <div class="col-lg-7">--}}
            {{--                            <div class="title">{{ $product->title }} </div>--}}
            {{--                            <div class="text">--}}
            {{--                                {{ $product->text }}--}}
            {{--                            </div>--}}
            {{--                            <div class="col-lg-5">--}}
            {{--                                <div class="price"> {{ $product->price }} ₸</div>--}}
            {{--                                <div class="count">--}}
            {{--                                    <span>1 порция </span>--}}
            {{--                                    <span>{{ $product->weight }}</span>--}}
            {{--                                </div>--}}
            {{--                            </div>--}}
            {{--                            <div class="col-lg-4">--}}

            {{--                                <div class="add-to-cart" ng-class="{flip: ct.cart['{{$product->id}}'].quantity == 0 || ct.cart['{{$product->id}}'].quantity != undefined}">--}}
            {{--                                    <div class="btn-wrap front">--}}
            {{--                                        <a href="#" class="btn-orange" ng-click="ct.addToCart(sl.items['{{$product->id}}']);$event.preventDefault()">--}}
            {{--                                            Заказать--}}
            {{--                                        </a>--}}
            {{--                                    </div>--}}

            {{--                                    <div class="quantity-control back">--}}
            {{--                                        <ul>--}}
            {{--                                            <li class="control-btn minus" ng-click="ct.minus(sl.items['{{$product->id}}'])"></li>--}}
            {{--                                            <li class="control-qty" ng-bind="ct.cart['{{$product->id}}'].quantity || 0"></li>--}}
            {{--                                            <li class="control-btn plus" ng-click="ct.addToCart(sl.items['{{$product->id}}']);$event.preventDefault()"></li>--}}
            {{--                                        </ul>--}}
            {{--                                    </div>--}}

            {{--                                </div>--}}

            {{--                            </div>--}}
            {{--                            <div class="clerfix"></div>--}}
            {{--                        </div>--}}
            {{--                        <div class="col-lg-5">--}}
            {{--                            <div class="image">--}}
            {{--                                <img src="{{ $product->main_image }}" alt="">--}}
            {{--                            </div>--}}
            {{--                        </div>--}}
            {{--                    </li>--}}
            {{--                @endforeach--}}
            {{--            </ul>--}}
            <div class="row">
                <ul class="items-slider mainslider">
                    @foreach($sliderItems as $sliderItem)
                        <li class="item">
                            <a href="{{$sliderItem->link}}" target="_blank">
                                <img src="{{ $sliderItem->image_url }}" alt="">
                            </a>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>


        <div class="popular-products" ng-controller="HomeController as hm">


            <div class="container">
                <div class="row no-padding">
                    <div class="catalog-menu hidden-xs hidden-sm hidden-md">
                        @include('layouts.main-menu')
                    </div>
                    <div class="col-lg-12">
                        <div class="title-page">  @lang('main.the_famous_dishes')</div>
                    </div>

                    <div class="products-list-index">

                        <div class="product simple col-lg-12 ng-scope" ng-repeat="product in  hm.popularProducts">
                            <div class="top">

                                <a href="/catalog/@{{ product.category.slug }}/@{{ product.slug }}">
                                    <div class="image">
                                        <img src="@{{ product.main_image }}" alt="">
                                    </div>
                                    <div class="title"><span ng-bind="product.title"></span></div>
                                </a>
                            </div>
                            <div class="clearfix"></div>
                            <div class="description">

                                <div class="text" ng-bind="product.text"></div>
                                <div class="footer row">

                                    <div class="col-lg-6 col-xs-6">
                                        <div class="price"> @{{ product.price }} ₸</div>
                                        <div class="count">
                                            <span>1 порция </span>
                                            <span> @{{ product.weight }}</span>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-xs-6">
                                        <div class="add-to-cart"
                                             ng-class="{flip: ct.cart[product.id].quantity == 0 || ct.cart[product.id].quantity != undefined}">
                                            <div class="btn-wrap front">
                                                <a href="#" class="btn btn-orange"
                                                   ng-click="ct.addToCart(product);$event.preventDefault()">
                                                    Заказать
                                                </a>
                                            </div>
                                            <div class="quantity-control back">
                                                <ul>
                                                    <li class="control-btn minus" ng-click="ct.minus(product)"></li>
                                                    <li class="control-qty">@{{ ct.cart[product.id].quantity || 0}}</li>
                                                    <li class="control-btn plus"
                                                        ng-click="ct.addToCart(product);$event.preventDefault()"></li>
                                                </ul>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>


                        {{--<div class="product top-left">--}}
                        {{--<div class="col-lg-7 col-md-12">--}}
                        {{--<div class="image">--}}
                        {{--<img src="/app/voyager/@{{ cg.topLeftProduct.image }}" alt="">--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="col-lg-5 col-md-12">--}}
                        {{--<div class="description">--}}
                        {{--<div class="top">--}}
                        {{--<div class="title"><a href="/catalog/@{{ cg.topLeftProduct.category.slug }}/@{{ cg.topLeftProduct.slug }}"><span ng-bind="cg.topLeftProduct.title"></span></a></div>--}}
                        {{--<div class="text" ng-bind="cg.topLeftProduct.text"></div>--}}
                        {{--</div>--}}
                        {{--<div class="footer row">--}}
                        {{--<div class="col-lg-6 col-xs-6">--}}
                        {{--<div class="price"> @{{ cg.topLeftProduct.price }} ₸</div>--}}
                        {{--<div class="count">--}}
                        {{--<span>1 порция </span>--}}
                        {{--<span> @{{ cg.topLeftProduct.size }}</span>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="col-lg-6 col-xs-6">--}}

                        {{--<div class="add-to-cart" ng-class="{flip: ct.cart[cg.topLeftProduct.id].quantity == 0 || ct.cart[cg.topLeftProduct.id].quantity != undefined}">--}}
                        {{--<div class="btn-wrap front">--}}
                        {{--<a href="#" class="btn" ng-click="ct.addToCart(cg.topLeftProduct);$event.preventDefault()">--}}
                        {{--Заказать--}}
                        {{--</a>--}}
                        {{--</div>--}}

                        {{--<div class="quantity-control back">--}}
                        {{--<ul>--}}
                        {{--<li class="control-btn minus" ng-click="ct.minus(cg.topLeftProduct)"></li>--}}
                        {{--<li class="control-qty">@{{ ct.cart[cg.topLeftProduct.id].quantity || 0}}</li>--}}
                        {{--<li class="control-btn plus" ng-click="ct.addToCart(cg.topLeftProduct);$event.preventDefault()"></li>--}}
                        {{--</ul>--}}
                        {{--</div>--}}
                        {{--</div>--}}


                        {{--</div>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="product top-right col-md-12">--}}
                        {{--<div class="image"><img src="/app/voyager/@{{ cg.topRightProduct.image }}" alt=""></div>--}}
                        {{--<div class="description">--}}
                        {{--<div class="title"><a href="/catalog/@{{ cg.topRightProduct.category.slug }}/@{{ cg.topRightProduct.slug }}"><span ng-bind="cg.topRightProduct.title"></span></a></div>--}}
                        {{--<div class="text" ng-bind="cg.topRightProduct.text"></div>--}}
                        {{--<div class="footer row">--}}

                        {{--<div class="col-lg-6 col-xs-6">--}}
                        {{--<div class="price"> @{{ cg.topRightProduct.price }} ₸</div>--}}
                        {{--<div class="count">--}}
                        {{--<span>1 порция </span>--}}
                        {{--<span> @{{ cg.topRightProduct.size }}</span>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="col-lg-6 col-xs-6">--}}

                        {{--<div class="add-to-cart" ng-class="{flip: ct.cart[cg.topRightProduct.id].quantity == 0 || ct.cart[cg.topRightProduct.id].quantity != undefined}">--}}

                        {{--<div class="btn-wrap front" ng-class="{flip: ct.cart[cg.topRightProduct.id].quantity == 0 || ct.cart[cg.topRightProduct.id].quantity != undefined}">--}}
                        {{--<a href="#" class="btn" ng-click="ct.addToCart(cg.topRightProduct);$event.preventDefault()">--}}
                        {{--Заказать--}}
                        {{--</a>--}}
                        {{--</div>--}}

                        {{--<div class="quantity-control back" >--}}
                        {{--<ul>--}}
                        {{--<li class="control-btn minus" ng-click="ct.minus(cg.topRightProduct)"></li>--}}
                        {{--<li class="control-qty">@{{ ct.cart[cg.topRightProduct.id].quantity || 0}}</li>--}}
                        {{--<li class="control-btn plus" ng-click="ct.addToCart(cg.topRightProduct);$event.preventDefault()"></li>--}}
                        {{--</ul>--}}
                        {{--</div>--}}
                        {{--</div>--}}


                        {{--</div>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        {{--</div>--}}

                        <div class="product simple col-lg-12" ng-repeat="product in ct.simpleProducts">
                            <div class="top">
                                <a href="/catalog/@{{ product.category.slug }}/@{{ product.slug }}">
                                    <div class="image">
                                        <img src="/app/voyager/@{{ product.image }}" alt="">
                                    </div>
                                    <div class="title"><span ng-bind="product.title"></span></div>
                                </a>
                            </div>
                            <div class="clearfix"></div>
                            <div class="description">

                                <div class="text" ng-bind="product.text"></div>
                                <div class="footer row">

                                    <div class="col-lg-6 col-xs-6">
                                        <div class="price"> @{{ product.price }} ₸</div>
                                        <div class="count">
                                            <span>1 порция </span>
                                            <span> @{{ product.size }}</span>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-xs-6">

                                        <div class="add-to-cart"
                                             ng-class="{flip: ct.cart[product.id].quantity == 0 || ct.cart[product.id].quantity != undefined}">
                                            <div class="btn-wrap front">
                                                <a href="#" class="btn"
                                                   ng-click="ct.addToCart(product);$event.preventDefault()">
                                                    Заказать
                                                </a>
                                            </div>

                                            <div class="quantity-control back">
                                                <ul>
                                                    <li class="control-btn minus" ng-click="ct.minus(product)"></li>
                                                    <li class="control-qty">@{{ ct.cart[product.id].quantity || 0}}</li>
                                                    <li class="control-btn plus"
                                                        ng-click="ct.addToCart(product);$event.preventDefault()"></li>
                                                </ul>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        {{--<div class="product bottom-left col-lg-12">--}}
                        {{--<div class="image">--}}
                        {{--<img src="/images/demo/bottom-left.png" alt="">--}}
                        {{--</div>--}}
                        {{--<div class="clearfix"></div>--}}
                        {{--<div class="description">--}}
                        {{--<div class="title"><span>Лагман дунганский </span></div>--}}
                        {{--<div class="text">--}}
                        {{--Куырдак готовится по оригинальному рецепту из нежной корейки баранины и--}}
                        {{--реберных частей.--}}
                        {{--Подается с сочным разноцветным перцем, обжаренной в специях картошкой, морковью--}}
                        {{--и зеленым луком.--}}
                        {{--</div>--}}
                        {{--<div class="footer">--}}
                        {{--<div class="col-lg-6">--}}
                        {{--<div class="price"> 1 400 ₸</div>--}}
                        {{--<div class="count">--}}
                        {{--<span>1 порция </span>--}}
                        {{--<span>450 гр.</span>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="col-lg-6">--}}
                        {{--<div class="btn-wrap">--}}
                        {{--<a href="#" class="btn"> Заказать</a>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        {{--</div>--}}


                    </div>

                    <div class="clearfix"></div>

                    {{-- 
                    <div class="reviews-block" ng-controller="ReviewsController as rv">
                        <div class="title-wrap">
                            <div class="title"> @lang('main.last_reviews') </div>
                        </div>

                        <div class="clearfix"></div>
                        <div class="recent-reviews">
                            @foreach($reviews as $review)
                                <div class="item">
                                    <div class="head">
                                        <div class="image"><img src="/{{ $review->user->avatar }}" alt=""></div>
                                        <div class="info-item">
                                            <div class="top">
                                                <div class="star">
                                                    <span class="rating-star-wrap">{{ $review->rating_stars }}</span>
                                                </div>
                                                <div class="time">{{ \LocalizedCarbon::instance($review->created_at)->diffForHumans() }}</div>
                                            </div>
                                            <div class="name">{{ $review->user->name }}</div>
                                            <div class="product-title"><a
                                                        href="/catalog/{{$review->product->category->slug}}/{{$review->product->slug}}">
                                                    о {{$review->product->title}} </a></div>
                                        </div>
                                    </div>
                                    <div class="text">
                                        {{ $review->text }}
                                    </div>

                                </div>
                            @endforeach
                        </div>
                    </div>
	
                    <div class="clearfix"></div>
					--}}

                </div>
            </div>
        </div>
    </div>

@endsection