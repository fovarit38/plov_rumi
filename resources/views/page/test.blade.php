@extends('layouts.page')

@section('content')
    {{ dd(\Cookie::has('city') == false) }}
    <div class="page static-page" ng-controller="IikoController as io">
        <div class="container">
            <div class="row no-padding">
                <div class="col-lg-12">
                    <div class="title-page"> Тестовая страница</div>

                    <div class="content">

                        <div class="col-lg-5">
                            <form ng-submit="io.createUser()">
                                <input type="submit" class="btn btn-default" value="Создать гостя">
                            </form>

                            <br/>

                            <form ng-submit="io.getNomenclature()">
                                <input type="submit" class="btn btn-default" value="Соединится">
                            </form>

                            <br>

                            <form ng-submit="io.getOrder()">
                                <input type="submit" class="btn btn-default" value="Получить заказ">
                            </form>

                            <br>

                            <button type="submit" class="btn btn-default" value="Создать заказ" ng-click="io.createOrder()"> Создать заказ </button>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    @if(!\Cookie::has('city'))
        <script>

            setTimeout(function(){
                $(document).ready(function () {
                    $('#citymodal').modal({
                        keyboard: false,
                        backdrop: 'static'
                    })
                });

            }, 3000);

        </script>

    @endif
@endsection
