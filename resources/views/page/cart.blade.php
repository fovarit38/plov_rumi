@extends('layouts.page')

@section('content')

    <div class="cart-page">
        <div class="container">
            <div class="row no-padding">
                <div class="col-lg-12">
                    <div class="title-page"> @lang('cart.title')</div>
                </div>

                <div >


                    <div class="col-lg-8">
                        <div ng-hide="!ct.isEmpty(ct.cart)"> <i> Корзина пуста  </i></div>
                        <div class="product" ng-repeat="product in ct.cart">
                            <div class="image inline-wrapper">
                                <img src="@{{ product.image }}" alt="">
                            </div>

                            <div class="info inline-wrapper">
                                <div class="title"> <span ng-bind="product.title"></span></div>
                                <div class="text" >1 порция, @{{ product.size }}</div>
                            </div>
                            <div class="product-count inline-wrapper">
                                <a href="#" ng-if="ct.cart[product.id].quantity != 1" ng-click="ct.minus(product);$event.preventDefault()"><span class="minus"> - </span></a>
                                <a href="#" ng-if="ct.cart[product.id].quantity == 1" ng-click="$event.preventDefault()"><span class="minus"> - </span></a>
                                <span class="count" ng-bind="ct.cart[product.id].quantity || 0"></span>
                                <a href="#" ng-click="ct.addToCart(product);$event.preventDefault()"><span class="plus"> + </span></a>
                            </div>

                            <div class="price inline-wrapper" ng-bind="ct.cart[product.id].quantity * ct.cart[product.id].price + ' ₸'"> </div>
                            <div class="delete inline-wrapper">
                                <a href="#" ng-click="ct.delete(ct.cart[product.id].id)">
                                    <img src="/images/icons/icon-del.svg" alt="" class="lg">
                                    <img src="/images/icons/del-xs.png" alt="" class="xs hide" >
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="block-summa" ng-show="!ct.isEmpty(ct.cart)">
                            <div class="title"> @lang('cart.total_amount'):</div>
                            <div class="product-count">
                                <div class="col-lg-6 col-md-6 col-xs-6">
                                    <div class="name"> @lang('cart.dishes_in_basket'):</div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-xs-6">
                                    <div class="count"> @{{ ct.cartProductCount }} блюд</div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="product-summ">
                                <div class="col-lg-6 col-md-6 col-xs-6">
                                    <div class="name"> @lang('cart.order_price'):</div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-xs-6">
                                    <div class="count" ng-bind="ct.cartsumm + ' ₸'"> </div>
                                </div>
                            </div>
                            <div class="btn-wrap">
                                <a href="/checkout" class="btn">Сделать заказ</a>
                            </div>
                        </div>
                    </div>

                </div>



            </div>
        </div>
    </div>


@endsection