@extends('layouts.cabinet')

@section('cabinet')
    <style>

    </style>
    <div class="settings">
        <div class="col-lg-6">
            <div ng-class="{'has-content': cb.user.name.length}" class="input username">
                <input type="text" ng-model="cb.user.name" placeholder="" class="input-std" ng-model-options="{updateOn: 'blur'}" ng-maxlength="35">
                <label for=""> @lang('cabinet.name'):</label>
            </div>

            <div ng-class="{'has-content': genderUser}" class="input gender">
                <select class="input-std" data-ng-options="o.name for o in cb.genders" data-ng-model="genderUser" >

                </select>
                <label for=""> @lang('cabinet.gender'): </label>
            </div>

            <div ng-class="{'has-content': cb.user.born}" class="input born">
                <select class="input-std" ng-model="cb.user.born" ng-options="year for year in cb.years">
                    <option value='' disabled selected></option>
                </select>
                <label for=""> @lang('cabinet.year_of_birth')</label>
            </div>
            <div class="input">
                <button class="btn btn-primary" data-toggle="modal" data-target="#avatarModal"><i class="glyphicon glyphicon-camera"></i> @lang('cabinet.change_photo')</button>


                <div id="avatarModal" class="modal fade" role="dialog">
                    <div class="modal-dialog modal-lg">

                        <div class="modal-content">
                            <div class="modal-header"></div>
                            <div class="modal-body">
                                <form name="avatarform">
                                    <div class="row">
                                        <div class="col-lg-6">

                                            <div ngf-drop ng-model="picFile" ngf-pattern="image/*" class="cropArea"
                                                 ngf-resize="{width: 200, height: 200}">
                                                <img-crop image="picFile  | ngfDataUrl" result-image="croppedDataUrl" ng-init="croppedDataUrl=''"></img-crop>
                                            </div>

                                            <button ngf-select ng-model="picFile" accept="image/*" class="btn btn-primary"> <i class="glyphicon glyphicon-camera"></i> Выберите файл </button>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="new-image">
                                                <img ng-src="@{{croppedDataUrl}}"/>
                                            </div>
                                            <button ng-click="cb.uploadAva(picFile)" class="btn btn-primary" > @lang('cabinet.save')</button>

                                            <span ng-show="result">Upload Successful</span>
                                            <span class="err" ng-show="errorMsg">@{{errorMsg}}</span>

                                        </div>
                                    </div>

                                </form>
                            </div>
                            <div class="modal-footer"></div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="input">
                <button class="btn btn-success" ng-click="cb.update()"> @lang('cabinet.save') </button>
            </div>


        </div>
    </div>

@endsection