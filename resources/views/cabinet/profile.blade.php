@extends('layouts.cabinet')

@section('cabinet')

    <div class="order-histories">
        @foreach($user->orders()->orderBy('id', 'desc')->get() as $order)

            <div class="order">
                <div class="info-wrapper">
                    <div class="order-number "> №{{ $order->number }}</div>
                    <div class="clearfix"></div>
                    <div class="count-product inline-wrapper" ><span>{{$order->products->count()}} @lang('main.dishes') на {{ $order->amount }}₸</span></div>
                    <div class="date inline-wrapper"> {{$order->date}}</div>
                    <div class="status {{ $order->status == true ? 'true' : 'false '}} inline-wrapper"> В обработке</div>
                    <div class="repeat inline-wrapper">
                        <div class="btn-wrap">
                            <a href="#" ng-click="ct.repeatOrder('{{$order->id}}')"> @lang('cabinet.repeat_order') </a>
                        </div>
                    </div>
                </div>
                <div class="products-list">
                    @foreach($order->products as $product )
                        <div class="product">
                            <a href="/catalog/{{$product->category->slug }}/{{$product->slug}}">
                                <div class="image inline-wrapper"><img src="{{ $product->main_image }}" alt=""></div>
                                <div class="info inline-wrapper">
                                    <div class="title"><span> {{ $product->title }} </span></div>
                                    <div class="text"> 1 @lang('main.portion'), {{ $product->weight }}</div>
                                </div>
                            </a>
                            <div class="count inline-wrapper"> {{ $product->pivot->qty }} шт </div>
                            <div class="summa inline-wrapper"> {{ $product->price * $product->pivot->qty }} ₸</div>
                        </div>
                    @endforeach
                </div>
            </div>
        @endforeach
    </div>




@endsection

