@extends('layouts.cabinet')

@section('cabinet')
    <div class="col-lg-6">
        <div ng-class="{'has-content': cb.user.phone.length}" class="input">
            <input type="text"
                   ng-model="cb.user.phone"
                   placeholder=""
                   class="input-std"
                   ng-model-options="{updateOn: 'blur'}">
            <label for=""> @lang('cabinet.phone') </label>
        </div>
        <div ng-class="{'has-content': cb.user.street.length}" class="input">
            <input type="text"
                   ng-model="cb.user.street"
                   placeholder=""
                    class="input-std"
                   ng-model-options="{updateOn: 'blur'}">
            <label for="">  @lang('cabinet.street')  </label>
        </div>

        <div ng-class="{'has-content': cb.user.home}" class="input">
            <input type="text"
                   ng-model="cb.user.home"
                    class="input-std"
                   ng-model-options="{updateOn: 'blur'}">
            <label for=""> @lang('cabinet.home') </label>
        </div>
        <div ng-class="{'has-content': cb.user.apartment.length}" class="input">
            <input type="text"
                   ng-model="cb.user.apartment"
                   placeholder=""
                    class="input-std"
                   ng-model-options="{updateOn: 'blur'}">
            <label for="">  @lang('cabinet.apartment') </label>
        </div>
        <div ng-class="{'has-content': cb.user.entrance.length}" class="input">
            <input type="text"
                   ng-model="cb.user.entrance"
                   placeholder=""
                    class="input-std"
                   ng-model-options="{updateOn: 'blur'}">
            <label for=""> @lang('cabinet.entrance') </label>
        </div>
        <div ng-class="{'has-content': cb.user.floor.length}" class="input">
            <input type="text"
                   ng-model="cb.user.floor"
                   placeholder=""
                  class="input-std"
                   ng-model-options="{updateOn: 'blur'}">
            <label for=""> @lang('cabinet.floor') </label>
        </div>
        <div ng-class="{'has-content': cb.user.doorphone.length}"  class="input">
            <input type="text"
                   ng-model="cb.user.doorphone"
                   placeholder=""
                    class="input-std"
                   ng-model-options="{updateOn: 'blur'}">
            <label for=""> @lang('cabinet.doorphone_code') </label>
        </div>

        <div class="input">
            <button ng-click="cb.update()" class="btn btn-success"> @lang('cabinet.save') </button>
        </div>
    </div>
@endsection