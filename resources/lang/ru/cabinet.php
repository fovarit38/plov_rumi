<?php


return [

    'title' => 'Личный кабинет',
    'history_of_orders' => 'История заказов',
    'address_and_phone' => 'Адрес и телефон',
    'settings_of_profile' => 'Настройка профиля',
    'repeat_order' => 'Повторить заказ',
    'posted_reviews' => 'Написал отзывов',
    'date_of_registration' => 'Дата регистрации',
    'my_orders' => 'Совершил покупок',
    
    'reviews' => 'отзывов',

    'phone' => 'Телефон ',
    'street' => 'Улица',
    'home' => 'Дом',
    'apartment' => 'Квартира',
    'entrance' => 'Подъезд',
    'floor' => 'Этаж',
    'doorphone_code' => 'Код домофона',
    'save' => 'Сохранить',
    'name' => 'Ваше имя',
    'gender' => 'Ваш пол',
    'date_of_birth' => 'Дата рождения',
    'year_of_birth' => 'Год рождения',
    'change_photo' => 'Изменить аватар',
	'comments' => 'например: нужна сдача с ..., информация о предзаказе ...',


];