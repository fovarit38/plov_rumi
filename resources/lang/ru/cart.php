<?php

return [

    'title' => 'Корзина',
    'total_amount' => 'Итого',
    'dishes_in_basket' => 'Блюда в корзине',
    'order_price' => 'Сумма заказа',
    'order_btn' => 'Сделать заказ'

];