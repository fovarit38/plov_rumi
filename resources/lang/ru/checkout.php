<?php


return [

    'title'      => 'Оформление заказа',
    'your_data'  => 'Ваши данные',
    'address_and_delivery' => 'Тип и адрес доставки',
    'courier_delivery' =>     'Доставка курьером ',
    'pickup' => 'Забрать самому ',

    'payment' => 'Оплата',
    'in_cash' => 'Наличными',
    'short_change' => 'Комментарий',

    'total_amount' => 'Итого',
    'order_price' => 'Сумма заказа',
    'shipping_amount' => 'Сумма доставки',
];