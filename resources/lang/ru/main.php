<?php


return [

    'menu'            => 'Меню',
    'delivery'        => 'Доставка',
    'sign_in'         => 'Войти',
    'registration'    => 'Регистрация',
    'city' => 'Город',
    'almaty' => 'Алматы',
    'astana' => 'Нур-Султан',
    
    'in_russian' => 'На русском',
    'in_english' => 'На английском',
    'in_kazakh' => 'На казахском',
    'site_lang' => 'Язык сайта',

    'email'    => 'E-mail',
    'password'    => 'Пароль',
    'social_auth'    => 'Или войдите через соцсеть:',


    'go_to_cart' => 'Перейти в корзину ',
    'сheckout' => 'Оформить заказ',


    'forgot_password' => 'Напомнить пароль',
    'my_profile'      => 'Мой профиль',
    'history_of_orders'      => 'История заказов',
    'settings_of_profile'      => 'Настройки профиля',
    'the_famous_dishes'      => 'Популярные блюда ',
    //'last_reviews'      => 'Последние отзывы',
    'logout'      => 'Выход',
    




    'copyright' => '© 2020 Онлайн-ресторан RUMI. Все права защищенны.',
    'delivery_information'      => 'Информация о доставке',
    'about_us'      => 'О нас',
    'contacts'      => 'Контакты',
    'developed'      => 'Разработка',
    
    
    'portion'      => 'порция',
    'dishes'      => 'блюда',

];