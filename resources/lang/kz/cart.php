<?php

return [

    'title' => 'Қоржын',
    'total_amount' => 'Барлығы',
    'dishes_in_basket' => 'Қоржындағы тағамдар',
    'order_price' => 'Тапсырыс сомасы',
    'order_btn' => 'Тапсырыс беру'
];