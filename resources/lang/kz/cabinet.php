<?php


return [

    'title' => 'Жеке кабинет',
    'history_of_orders' => 'Тапсырыс тарихы',
    'address_and_phone' => 'Мекен-жай және телефон',
    'settings_of_profile' => 'Бейінді баптау',
    'repeat_order' => 'Тапсырысты қайталау',
    'posted_reviews' => 'Қалдырған пікірім ',
    'date_of_registration' => 'Тіркелу күні',
    'my_orders' => 'Сатып алдым',

    'reviews' => 'пікір',

    'phone' => 'Телефон ',
    'street' => 'Көше',
    'home' => 'Үй',
    'apartment' => 'Пәтер',
    'entrance' => 'Подъезд',
    'floor' => 'Қабат',
    'doorphone_code' => 'Домофон коды',
    'save' => 'Сақтау',
    'name' => 'Есіміңіз',
    'gender' => 'Жынысыңыз',
    'date_of_birth' => 'Дата рождения',
    'year_of_birth' => 'Туған жылыңыз',
    'change_photo' => 'Аватарды өзгерту',
	'comments' => 'Мысалы: алдын ала тапсырыс беру, қажетті қайтарым соммасы ...',
];