<?php


return [

    'title'      => 'Тапсырысты рәсімдеу',
    'your_data'  => 'Сіздің деректеріңіз',
    'address_and_delivery' => 'Жеткізу мекен-жайы мен түрі',
    'courier_delivery' =>     'Доставка курьером ',
    'pickup' => 'Забрать самому ',
    
    'payment' => 'Төлеу',
    'in_cash' => 'Қолма-қол төлеу',
    'short_change' => 'Қосымша ақпарат',

    'total_amount' => 'Барлығы',
    'order_price' => 'Тапсырыс сомасы',
    'shipping_amount' => 'Жеткізу сомасы',

];