<?php


return [

    'menu'            => 'Мәзір',
    'delivery'        => 'Жеткізу',
    'sign_in'         => 'Кіру',
    'registration'    => ' Тіркелу ',
    'city' => 'Қала',
    'almaty' => 'Алматы',
    'astana' => 'Нур-Султан',
    

    'in_russian' => 'На русском',
    'in_english' => 'На английском',
    'in_kazakh' => 'На казахском',
    'site_lang' => 'Сайт тілі',
    
    'email'    => 'E-mail',
    'password'    => 'Құпиясөз',
    'social_auth'    => 'Немесе әлеуметтік желі арқылы кіру:',
    'go_to_cart' => 'Қоржынға өту',
    'сheckout' => 'Тапсырысты орындау',

    'forgot_password' => 'Құпиясөзді еске салу',
    'my_profile'      => 'Менің бейінім',
    'history_of_orders'      => 'Тапсырыс тарихы',
    'settings_of_profile'      => 'Бейінді баптау',
    'the_famous_dishes'      => 'Танымал тағамдар',
    //'last_reviews'      => 'Соңғы пікірлер',
    'logout'      => 'Шығу',

    'copyright' => '© 2020 Онлайн-мейрамхана RUMI. Барлық құқықтар сақталған.',
    'delivery_information'      => 'Жеткізу жайлы ақпарат',
    'about_us'      => 'Біз жайлы',
    'contacts'      => 'Байланыс',
    'developed'      => 'Дайындау',

    'portion'      => 'порция',
    'dishes'      => 'блюда',

];