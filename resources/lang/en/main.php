<?php


return [

    'menu'            => 'Menu',
    'delivery'        => 'Delivery',
    'sign_in'         => 'Sign in',
    'registration'    => 'Registration',
    'city' => 'City',
    'almaty' => 'Almaty',
    'astana' => 'Nur-Sultan',
    'site_lang' => 'Language',

    'in_russian' => 'in Russian',
    'in_english' => 'in English',
    'in_kazakh' => 'in Kazakh',

    'email'    => 'E-mail',
    'password'    => 'Password',
    'social_auth'    => 'Sign in with',
    'go_to_cart' => 'Go to my cart',
    'сheckout' => 'Сheckout',


    'forgot_password' => 'Forgot password',
    'my_profile'      => 'My profile',
    'history_of_orders'      => 'History of orders',
    'settings_of_profile'      => 'Settings of profile',
    'the_famous_dishes'      => 'The famous dishes',
    //'last_reviews'      => 'Last reviews',
    'logout'      => 'Выход',


    'copyright' => '© 2020 Rumi online restaurant. All rights reserved',
    'delivery_information'      => 'Delivery information',
    'about_us'      => 'About us',
    'contacts'      => 'Contacts',
    'developed'      => 'Developed',
    
    'portion'      => 'portion',
    'dishes'      => 'dishes',


];