<?php

return [

    'title' => 'Cart',
    'total_amount' => 'Total amount',
    'dishes_in_basket' => 'Dishes in basket',
    'order_price' => 'Order price',
    'order_btn' => 'Сделать заказ'

];