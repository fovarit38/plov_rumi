<?php


return [

    'title'      => 'Оформление заказа',
    'your_data'  => 'Ваши данные',
    'address_and_delivery' => 'Тип и адрес доставки',
    'courier_delivery' =>     'Доставка курьером ',
    'pickup' => 'Забрать самому ',
    
    'payment' => 'Payment',
    'in_cash' => 'In cash',
    'short_change' => 'Comments',

    'total_amount' => 'Total',
    'order_price' => 'Order price',
    'shipping_amount' => 'Shipping amount',
];