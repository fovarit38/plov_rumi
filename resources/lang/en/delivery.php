<?php

return [


    'title' => 'Delivery',

    'block_title_1' => 'Terms of Delivery',
    'block_title_2' => 'The time of delivery',
    'block_title_3' => 'Minimum amount for order is',

    'first_section' => [

        'bold_title' => 'In the area between next streets',
        'text' => 'Raimbek Avenue -  Eastern Bypass Road - Al Farabi Avenue (included Kazhimukana) - Momishuly ',

        'info_summa' => [
            'summa' => '1000 tenge',
            'caption' => 'If your order will be cost more than 10000 tenge, the delivery will be for free.',
        ],
        'info_time'=>[
            'summa' => '40-90 min',
            'caption' => 'Raimbek Avenue -  Eastern Bypass Road - Al Farabi Avenue (included Kazhimukana) - Momishuly ',
        ]
    ],

    'second_section' => [
        'bold_title' => 'Outside of above streets',
        'info_summa' => [
            'summa' => 'started from 1000 tg',
            'caption' => 'The operator will give exact cost to you during your order.',
        ],
        'info_time'=>[
            'summa' => '90/120 min ',
            'caption' => 'The time of delivery depends on distance and traffic.',
        ]
    ],
    
    'third_section' => [
        'bold_title' => 'Go to pick up:',
        'info_address' => [
            'summa' => 'You can pick up the order  by yourself from the following addresses: Абылай хана 92, ТРЦ Москва',
            'caption' => 'From 10.00-23.45',
        ],
        'info_time'=>[
            'summa' => 'from 10.00-22.00',
            'caption' => 'The time of delivery',
        ]
    ],
    
    'fourth_section' =>  [
        'info_summa' => [
            'summa' => '3 000 ₸',
            'caption' => 'The cost for disposable dishes are  50 tenge per unit.',
        ]
    ]

];