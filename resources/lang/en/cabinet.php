<?php


return [
    
    'title' => 'Personal profile',
    'history_of_orders' => 'History of orders',
    'address_and_phone' => 'Address and phone',
    'settings_of_profile' => 'Settings of profile',
    'repeat_order' => 'Repeat order',
    'posted_reviews' => 'Posted reviews',
    'date_of_registration' => 'Date of registration',
    'my_orders' => 'My orders',

    'reviews' => 'reviews',

    'phone' => 'Phone ',
    'street' => 'Street',
    'home' => 'House',
    'apartment' => 'Apartment',
    'entrance' => 'Entrance',
    'floor' => 'Floor',
    'doorphone_code' => 'Doorphone code',
    'save' => 'Save',
    'name' => 'Name',
    'gender' => 'Gender',
    'date_of_birth' => 'Date of birth',
    'year_of_birth' => 'Year of birth',
    'change_photo' => 'Change photo',
	'comments' => 'some comments here ...',
];