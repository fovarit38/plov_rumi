<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(['middleware' => ['siteDisable']], function () {

    Route::get('api/auth', 'TestController@auth');

    Route::get('auth/{provider}', 'Auth\RegisterController@redirectToProvider');
    Route::get('auth/{provider}/callback', 'Auth\RegisterController@handleProviderCallback');

    Route::get('/', 'HomeController@index');
    Route::get('cabinet', 'CabinetController@index');
    Route::get('cabinet/profile', 'CabinetController@history');
    Route::get('cabinet/contacts', 'CabinetController@contacts');
    Route::get('cabinet/settings', 'CabinetController@settings');

    Route::get('user/get', 'Api\CabinetController@user');
    Route::post('user/update', 'Api\CabinetController@update');

    Route::post('user/avatar/upload', 'Api\CabinetController@uploadAvatar');
    Route::post('user/{id}/avatar/delete', 'Api\CabinetController@deleteAvatar');

    Route::post('/new/order', 'OrderController@newOrder');
    Route::post('repeat/order/{id}', 'OrderController@repeatOrder');
    Route::post('/product/{id}/add', 'OrderController@addSingle');

    Route::get('order/{id}/accepted', ['as' => 'orderaccepted', 'uses' => 'OrderController@accepted']);

    /* payment */

    Route::get('confirmation', ['as' => 'orderconfirm', 'uses' => 'OrderController@confirmation']);
    Route::post('kkb/postlink', 'OrderController@postlink');

    Route::get('cart', 'CartController@index');
    Route::get('delivery', 'PageController@delivery');
    Route::get('checkout', 'PageController@checkout');

    Route::get('about', 'PageController@about');
    Route::get('contacts', 'PageController@contacts');
    Route::get('test', 'PageController@test');

    Route::get('product/single', 'CatalogController@single');
    Route::get('registration', 'UserController@registration');
    Route::get('password/reset', 'UserController@resetPass');

    Route::get('catalog/{slug}', ['as' => 'catalogIndex', 'uses' => 'CatalogController@index']);
    Route::get('catalog/{category_slug}/{slug}', 'CatalogController@single');

    Route::get('/type/product/get', 'Api\CatalogController@getProductByType');

    Route::post('user/auth', 'Auth\LoginController@login');
    Route::get('user/logout', 'Auth\LoginController@logout');

    Route::get('products/category/{id}/get', 'Api\CatalogController@getProducts');
    Route::get('popular/products/get', 'HomeController@popularProducts');


    Route::get('product/{id}/reviews/get', 'Api\ReviewController@getReviews');
    Route::post('product/{id}/review/save', 'Api\ReviewController@save');
    Route::get('recent/reviews/get', 'Api\ReviewController@recent');

    Route::get('slider/products/get', 'Api\CatalogController@slider');
    Route::get('product/{id}/recommend/get', 'Api\CatalogController@recommend');

    Route::get('get/nomenclature/{city}', 'Api\CatalogController@getNomenclature');
    Route::post('get/order', 'Api\CatalogController@getOrderInfo');
    Route::post('create/order', 'Api\CatalogController@createOrder');

    Route::get('{object_type}/{id}/images/get', 'ImageController@getImages');
    Route::get('{object_type}/{id}/image/get', 'ImageController@getImage');
    Route::post('{object_type}/{object_id}/image/upload', 'ImageController@upload');
    Route::post('{object_type}/{object_id}/image/{image_id}/delete', 'ImageController@deleteGalleryImg');

    Route::post('user/create', 'OrderController@createUser');
    Route::post('change/city', 'CityController@change');


    Route::get('apply-promocode', 'PromocodeController@getPromocode');
});


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();

    Route::get('integration', 'Admin\IntegrationController@index');
    Route::get('products-almaty', 'Admin\ProductController@index');
    Route::get('products-astana', 'Admin\ProductController@index');
    Route::get('shop-2', 'Admin\ProductController@shop_2');

});

Auth::routes();

Route::post('/language', array(
    'Middleware' => 'Locale',
    'uses' => 'LanguageController@index'
));
