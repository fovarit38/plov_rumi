<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    'facebook' => [
        'client_id' => '1984532361777958',
        'client_secret' => '41c733bb28c2be8be176d44790afc952',
        'redirect' => 'http://plov.kz/auth/facebook/callback',
    ],

    'google' => [ //change it to any provider
        'client_id' => '796704995734-tq67kkta92bm6kclsmoqo9mcvj4fqjqp.apps.googleusercontent.com',
        'client_secret' => 'Qc9jnogzdM_yRugPU0FtE538',
        'redirect' => 'http://plov.kz/auth/google/callback',
    ],

    'vkontakte' => [
        'client_id' => '6082044',
        'client_secret' => '5YIybz5txvXcMUA6SOuv',
        'redirect' => 'http://plov.kz/auth/vkontakte/callback',
    ],

];
