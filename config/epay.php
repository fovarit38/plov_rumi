<?php
return [
    'pay_test_mode' => false,

    'MERCHANT_CERTIFICATE_ID'   => 'c183eeba',
    'MERCHANT_NAME'             => 'plov.kz',
    'PRIVATE_KEY_PATH'          => '../app/kkb/cert.prv',
    'PRIVATE_KEY_PASS'          => 'WDfUveEf9i3',
    'XML_TEMPLATE_FN'           => '../app/kkb/template.xml',
    'XML_COMMAND_TEMPLATE_FN'   => '../app/epay/command_template.xml',
    'PUBLIC_KEY_PATH'           => '../app/kkb/kkbca.pem',
    'MERCHANT_ID'               => '93162311',
    // Линк для возврата покупателя в магазин (на сайт) после успешного проведения оплаты
    'EPAY_BACK_LINK'            => 'https://plov.kz',
    // Линк для отправки результата авторизации в магазин.
    'EPAY_POST_LINK'            => 'https://plov.kz/kkb/postlink',
    // Линк для отправки неудачного результата авторизации либо информации об ошибке в магазин.
    'EPAY_FAILURE_POST_LINK'    => 'https://plov.kz',

    'EPAY_FORM_TEMPLATE'        => 'default.xsl',
];